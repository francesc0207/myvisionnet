﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;


namespace MvcApplication1.Models
{
    
    public class ProjectUser : DbContext
    {
        public ProjectUser()
            : base("DefaultConnection")
        {
        }

    }

    public class Model
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }

    public class Repository
    {
        public static List<Model> GetData()
        {
            List<Model> list = new List<Model>
            {
                new Model
                {
                    ID = 1,
                    Name = "Rohit",
                    ImageUrl = "../../Images/accent.png"
                },
                new Model
                {
                    ID = 2,
                    Name = "Arun",
                    ImageUrl = "../../Images/bullet.png"
                },
                new Model
                {
                    ID = 3,
                    Name = "Rahul",
                    ImageUrl = "../../Images/play_button.jpg"
                }
            };

            return list;
        }
    }







    public class Proyecto
    {
                public int Id { get; set; }
                public string Nombre { get; set; }
                public int IdUser { get; set; }
    }

    public class ProyList
    {
        public IEnumerable<Proyecto> Proyectos { get; set; }
    }

    public class usrId
    {
        public IEnumerable<int> Idopuser { get; set; }
    }

  
    public class Carpeta
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int IdParent { get; set; }

    }

   public class CarpList{

    public IEnumerable<Carpeta> Carpetas {get;set;}
   }

   public class Proceso
   {
       public int Id { get; set; }
       public string Nombre { get; set; }

   }

   public class ProcList
   {

       public IEnumerable<Proceso> Procesos { get; set; }
   }
}