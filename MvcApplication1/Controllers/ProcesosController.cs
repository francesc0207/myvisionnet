﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    public class ProcesosController : Controller
    {
        private DCREntities db = new DCREntities();

        //
        // GET: /Procesos/

        public ActionResult Index()
        {
            return View(db.BTPROCESSEXCELREPORT.ToList());
        }

        public IEnumerable<BTITEM > Procesos_Listar(Int32 idfolder)
        {
       
        //Falta aplicar:
        //Case eSecurityMode.TreeSecurity, eSecurityMode.TotalAccessSecurity
        //    sError = "Error: El modelo de seguridad del usuario no está soportado"
        //End Select
            
        var myList = new List<string>(AccountController.GetPerm(User).Split(','));
        IEnumerable<int> ListIDs = myList.Select(int.Parse).ToList();
        var usrs = from k in ListIDs select new OPUSER  {IDUSUARIO  = k };
    
          
        IEnumerable<int> v2 = (from c1 in
                                      ((from c in db.BTSUBJECT_ITEMS select c).ToList())
                                      join c2 in db.BTITEMUSER
                                      on c1.IdItem
                                      equals c2.IDITEM where (c1.IdSubject == idfolder)
                                      join c3 in usrs on c2.IDUSER equals c3.IDUSUARIO
                                      select c1.IdItem).Distinct().ToList();

        IEnumerable<BTPROCESS> v3 = (from m in v2 join t in db.BTPROCESS on m equals t.IDPROCESS
                                 where (t.PROCESSCLASS == 12 | t.PROCESSCLASS == 2 | t.PROCESSCLASS == 11) && t.MyVisionVisible.Equals(true)
                                 select t).ToList();

        IEnumerable<BTITEM> procesoss = (from m in v3
                                                   join r in db.BTITEM on m.IDPROCESS  equals r.ID
                                                   select new BTITEM { ID  = r.ID, LOGICAL_NAME  = r.LOGICAL_NAME  }).Distinct().ToList();
                        return procesoss.AsEnumerable();
        }

        public ActionResult Procesos(Int32 idfolder,Int32 idfolderPar=0, bool LevelUP=false)
        {
            int idfparent;
            BTSUBJECT carp = db.BTSUBJECT.Where(c => c.IdSubjectParent == idfolder).FirstOrDefault();
            var idprj = db.RPPROJECTITEM.Where(p => p.ID == idfolder).FirstOrDefault().IDPROJECT;
            if (carp != null)
            {
                idfparent = carp.IdSubjectParent.Value;
                if (idfparent > 0)  //Si contiene subcarpetas
                {                   
                    return RedirectToAction("SubCarpetas", "BtSubjects", new { idprj = idprj, idpar= idfparent });
                }
            }

            BTSUBJECT_ITEMS idfolderP = db.BTSUBJECT_ITEMS.Where(a => a.IdItem == idfolder).FirstOrDefault();
            int idfolderProc = idfolder;
            if(idfolderP != null) { idfolderProc = idfolderP.IdSubject; }
            if (LevelUP) { idfolder = idfolderProc; };
            ViewData["prj"] = idprj;
            ViewData["carp"] = idfolder;
            if (idfolderPar == 0)
            {
                return View(Procesos_Listar(idfolder));
            }else
            {
                ViewData["carp"] = idfolderPar;
                return View(Procesos_Listar(idfolderPar));
            }


        }

        // GET: /Procesos/Details/5

        public ActionResult Details(int id = 0)
        {
            BTPROCESSEXCELREPORT btprocessexcelreport = db.BTPROCESSEXCELREPORT.Find(id);
            if (btprocessexcelreport == null)
            {
                return HttpNotFound();
            }
            return View(btprocessexcelreport);
        }

        //
        // GET: /Procesos/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Procesos/Create

        [HttpPost]
        public ActionResult Create(BTPROCESSEXCELREPORT btprocessexcelreport)
        {
            if (ModelState.IsValid)
            {
                
                db.BTPROCESSEXCELREPORT.Add(btprocessexcelreport);
                db.SaveChanges();
              return RedirectToAction("Index");
            }

            return View(btprocessexcelreport);
        }

        //
        // GET: /Procesos/Run/5

        public ActionResult Run(string sParams,int id = 0 )
        {

                    string sMachineName = "PC"; //GetThisMachineName()
                    string sError = "";
                    //Demanem un nou ID per al job
                    int newJobID = GetFreeID(7, sError);
                    if (sError != "")
                    {
                        return RedirectToAction("Index");
                    };
                    string sIdUser = AccountController.GetUsrId(User).ToString();
                    string sParamString = sParams == null ? "" : sParams;
                    Int32 JobWaiting=0;
                    string TEST_ASAP = "TEST_PROCESS_ASAP";
   
                    string ssql = "INSERT INTO OPJOB(idJob , iduser, idServer, idPlan, IDProcess, idParentJob, Rootjob, Depth, EXECUTIONSTATUS, Trace_Level, UserParams)";
                    ssql = ssql + "VALUES (" + newJobID + "," + sIdUser + ",0, 0, " + id + ",0," + id + ",0, " + JobWaiting.ToString() + ",0, '" + sParamString.Replace("'", "''") + "') ";

                    int  newMessageID = GetFreeID(3, "");
                    //'Planifiquem el proces ASAP
                    string sInsertMessageSQL = "INSERT INTO COMUNICATOR (IDMessage, Moment, IDServer, Command, Param1, Param2, Param3, Priority, OriginIdUser, OriginMachineName, ServerLock)";
                    sInsertMessageSQL = sInsertMessageSQL + "VALUES (" + newMessageID + ",GetDate(),0,'" + TEST_ASAP + "','" + id + "','" + newJobID +"','" + sParamString.Replace("'", "''") + "',0," + sIdUser + ",'" + sMachineName + "',0)";
   

                    db.OPJOB.SqlQuery(ssql);
                    db.Database.ExecuteSqlCommand(ssql);
                    db.SaveChanges();
                    db.COMUNICATOR.SqlQuery(sInsertMessageSQL);
                    db.Database.ExecuteSqlCommand(sInsertMessageSQL);
                    db.SaveChanges();
            
                    return View();
        }
        

        [HttpPost]
        public ActionResult Run(BTPROCESSEXCELREPORT btprocessexcelreport)
        {
            if (ModelState.IsValid)
            {
                db.Entry(btprocessexcelreport).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(btprocessexcelreport);
        }


        //
        // GET: /Procesos/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BTPROCESSEXCELREPORT btprocessexcelreport = db.BTPROCESSEXCELREPORT.Find(id);
            if (btprocessexcelreport == null)
            {
                return HttpNotFound();
            }
            return View(btprocessexcelreport);
        }

        //
        // POST: /Procesos/Edit/5

        [HttpPost]
        public ActionResult Edit(BTPROCESSEXCELREPORT btprocessexcelreport)
        {
            if (ModelState.IsValid)
            {
                db.Entry(btprocessexcelreport).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(btprocessexcelreport);
        }

        //
       

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public int  GetFreeID(int a, string e)
        {
            int rid;
            rid = -1;
            try
            {

                MAXIMO maxi = db.MAXIMO.SingleOrDefault(c=>c.IDTABLA== a);
                rid = maxi.MAXIMO1.Value;                             
                db.MAXIMO.Single(c=>c.IDTABLA== a).MAXIMO1  = rid+1;
                db.SaveChanges();
                
            }

            catch (Exception ex) 
            {
                e = ex.Message.ToString(); 

            }
            
            return rid;

        }
    }
}