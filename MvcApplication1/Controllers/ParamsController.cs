﻿using MvcApplication1.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{

    public enum eDatabaseType
    {
        DB_TYPE_ACCESS = 1,     //Access
        DB_TYPE_TEXT,           //Fitxer texte
        DB_TYPE_EXCEL,          //Fitxer excel
        DB_TYPE_ODBC,           //Connexió via ODBC
        DB_TYPE_DBASE_III,      //Connexió a dBase III
        DB_TYPE_DBASE_IV,       //Connexió a dBase IV
        DB_TYPE_DBASE_V,        //Connexió a dBase V
        DB_TYPE_WK4,            //Connexió a fitxer Lotus 1-2-3
        DB_TYPE_EXCEL2007,      //Connexió a EXCEL 2007
        DB_TYPE_ACCESS2007,     //Connexió a ACCESS 2007
        DB_TYPE_EXCEL2010,      //Connexió a EXCEL 2010
        DB_TYPE_ACCESS2010      //Connexió a ACCESS 2010
    }
    
    public class ParamsController : Controller
    {

        //    Retorna el string de conexió a la base de dades a partir de l'identificador de la base de dades

        private Int32 sIdDatabaseQuery;
        private string GetODBCConnectionString(int sConnectionModus, string sConnectionstring, string sOdbcDSN, string sODBCDriver, string sLogin, string sPassword, string sOdbcServerName, string sOdbcDatabaseName, string sError)

        {
            string conStr = "";

            switch (sConnectionModus)
            {
                case 1: //Modus_ConnectionString
                    conStr = sConnectionstring;
                    conStr = conStr + "UID=" + sLogin + ";";
                    conStr = conStr + "PWD=" + sPassword + ";";
                    return conStr;

                case 2: // Modus_ConnectToDSN
                    conStr = "DSN=" + sOdbcDSN + ";";
                    conStr = conStr + "UID=" + sLogin + ";";
                    conStr = conStr + "PWD=" + sPassword + ";";
                    conStr = conStr + "OLE DB Services= -2;";       //Això és basic per evitar el Pooling (caché) de conexions
                    return conStr;
                case 3: // Modus_ConnectToDriver
                    //conStr = "Driver={" + sODBCDriver + "};";
                    conStr = conStr + "Server=" + sOdbcServerName + ";";
                    conStr = conStr + "Database=" + sOdbcDatabaseName + ";";
                    conStr = conStr + "UID=" + sLogin + ";";
                    conStr = conStr + "PWD=" + sPassword + ";";
                    return conStr;
                default:
                    return conStr;
            }

        }


        public string GetConnectionString(int sIDDataBase, int sDataEngine, string sError)
        {

            var conxDatos = (from x in db.BTDATABASE
                             where (x.IDDATABASE == sIDDataBase)
                             select x).ToList();
            int sDatabaseType, sConnectionModus;
            string sConnectionStringOptions, sFileName;
            string sSystemFile, sLogin, sPassword, sConnectionString, sOdbcDSN, sODBCDriver, sOdbcServerName, sOdbcDatabaseName;
            if (conxDatos != null)
            {
                try
                {
                    sConnectionModus = conxDatos.LastOrDefault().ConnectModus.Value;
                    sDatabaseType = conxDatos.LastOrDefault().SGBD.Value;
                    sFileName = conxDatos.LastOrDefault().FilePath.ToString();
                    sSystemFile = conxDatos.LastOrDefault().CONNSYSTEM.ToString();
                    sLogin = conxDatos.LastOrDefault().CONNLOGIN.ToString();
                    sPassword = conxDatos.LastOrDefault().CONNPASSWORD.ToString();
                    sConnectionString = conxDatos.LastOrDefault().CONNSTRCONNECTION.ToString();
                    sOdbcDSN = conxDatos.LastOrDefault().CONNPASSWORD.ToString();
                    sODBCDriver = conxDatos.LastOrDefault().ConnectDriver.ToString();
                    sOdbcServerName = conxDatos.LastOrDefault().SERVIDOR.ToString();
                    sOdbcDatabaseName = conxDatos.LastOrDefault().DatabaseName.ToString();
                    
                    switch ((eDatabaseType)sDatabaseType)
                    {

                        case eDatabaseType.DB_TYPE_ODBC:
                            sError = "";
                            return GetODBCConnectionString(sConnectionModus, sConnectionString, sOdbcDSN, sODBCDriver, sLogin, sPassword, sOdbcServerName, sOdbcDatabaseName, sError);
                            
                        case eDatabaseType.DB_TYPE_TEXT:
                            sError = "";
                            return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + sFileName + ";Extended Properties='text';";
                        case eDatabaseType.DB_TYPE_EXCEL:
                            sError = "";
                            if (sDataEngine == 1) { sConnectionStringOptions = "Excel 8.0;HDR=NO;IMEX=2"; }
                            else
                            { //dao350
                                sConnectionStringOptions = "Excel 8.0;HDR=YES;IMEX=2";
                            }

                            return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + sFileName + ";Extended Properties=" + sConnectionStringOptions + "";
                        case eDatabaseType.DB_TYPE_DBASE_III:
                        case eDatabaseType.DB_TYPE_DBASE_IV:
                        case eDatabaseType.DB_TYPE_DBASE_V:
                        case eDatabaseType.DB_TYPE_WK4:
                            sError = "";
                            return "";
                        case eDatabaseType.DB_TYPE_ACCESS:
                            if (sSystemFile.Length > 0) { return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + sFileName + ";"; }
                            else
                            { return "Provider=Microsoft.Jet.OLEDB.4.0;User ID=" + sLogin + ";Password=" + sPassword + ";Data Source=" + sFileName + ";"; }

                        case eDatabaseType.DB_TYPE_EXCEL2007:
                        case eDatabaseType.DB_TYPE_EXCEL2010:
                            sConnectionStringOptions = "Excel 12.0;HDR=NO;IMEX=0";
                            return "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + sFileName + ";Extended Properties='" + sConnectionStringOptions + "'";
                        case eDatabaseType.DB_TYPE_ACCESS2007:
                        case eDatabaseType.DB_TYPE_ACCESS2010:
                            return "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + sFileName;
                        default:
                            return "";
                    }
                }
                catch (Exception ex)
                {
                    sError = ex.Message + ",No se ha encontrado un perfil de base de datos con Id=" + sIDDataBase;
                    return "";
                }
            }

            else
            {
                sError = "No se ha encontrado un perfil de base de datos con Id=" + sIDDataBase;
                return "";
            }
        }


        private DCREntities db = new DCREntities();


        private MultiSelectList GetParamValues(Int32 idparam)
        {
            var ParamValues = (from x in
                                   ((from y in db.BTITEMPARAM select y).ToList())
                               join z in db.BTPARAMTYPEDOMAINVALUES
                               on x.ParamType
                               equals z.IdParamType
                               where (x.IdParam == idparam)
                               select z).ToList();


            return new MultiSelectList(ParamValues, "DomainValue", "ValueUserText");
        }

        public MultiSelectList GetParamValuesFromDB(Int32 idqCall)
        {

            try {
                Dictionary<string, string> ParamValuesDB = new Dictionary<string, string>();
                var queryData = db.BTQUERYCALL.Where(p => p.IdQueryCall == idqCall);
                var queryData2 = db.BTQUERY.Where(q => q.IDQUERY == queryData.FirstOrDefault().IdQuery).FirstOrDefault();

                string conx = GetConnectionString(queryData2.IDDATABASE.Value, queryData2.DATA_ENGINE.Value, "");
                
                string sError = "";
                string sParamString = "";
                Collection<Param> ParamSet;
                ParamSet = DecompressParam(sParamString, ref sError);

                //return null;

                LoadParamsOfItemFromDB(queryData2.IDQUERY, idqCall, ParamSet);
                DateTime moment = DateTime.Now;
                DefineTimeContextParams(moment, ref ParamSet);     //Estableix el valors de tots els paràmetres que dependen del temps.

                //sIdDatabaseQuery = sIDDataBase

                string sSQL = queryData2.QUERY.ToString();
                var queryData3 = db.RPPROJECTITEM.Where(p => p.ID == queryData2.IDQUERY).FirstOrDefault();

                int UserId = AccountController.GetUsrId(User);
                sSQL = Preprocess(sSQL, ParamSet, UserId.ToString(), queryData3.IDPROJECT, sError);
                
                if (conx.Contains("OLEDB"))
                {

                    System.Data.OleDb.OleDbConnection sqlconn = new System.Data.OleDb.OleDbConnection(conx);
                    System.Data.OleDb.OleDbCommand sqlcmd = new System.Data.OleDb.OleDbCommand(sSQL, sqlconn);
                    using (sqlconn)
                    {
                        sqlconn.Open();
                        using (System.Data.OleDb.OleDbDataReader reader = sqlcmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.FieldCount == 1)
                                {
                                    ParamValuesDB.Add(Convert.ToString(reader[1]), Convert.ToString(reader[1]));
                                }
                                else
                                {
                                    ParamValuesDB.Add(Convert.ToString(reader[1]), Convert.ToString(reader[2]));
                                }
                            }
                        }
                    }
                }
                else
                {
                    System.Data.SqlClient.SqlConnection sqlconn = new System.Data.SqlClient.SqlConnection(conx);
                    System.Data.SqlClient.SqlCommand sqlcmd = new System.Data.SqlClient.SqlCommand(sSQL, sqlconn);

                    using (sqlconn)
                    {
                        sqlconn.Open();
                        using (System.Data.SqlClient.SqlDataReader reader = sqlcmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.FieldCount == 1)
                                {
                                    ParamValuesDB.Add(Convert.ToString(reader[0]), Convert.ToString(reader[0]));
                                }
                                else { ParamValuesDB.Add(Convert.ToString(reader[0]), Convert.ToString(reader[1])); }
                            }
                        }
                    }

                }
                return new MultiSelectList(ParamValuesDB, "Key", "Value");
            }
            catch  (Exception ex)
{
                    // Add useful information to the exception
                    throw new ApplicationException("Imposible conectar al origen de datos para obtemener los valores de parametros:", ex);
                }




           
        }

        public string Preprocess(string sSQL, Collection<Param> paramSet, string sIdUser, int iDPROJECT, string sError)
        {
            String  ParamNameTemp, ParamValueTemp,  sIdDatabaseQuery_str;
            //Param aParam;
            try {
            

                foreach (Param aParam in paramSet) {
                    sSQL = SubsToken(sSQL, "@FIELD(" + aParam.ParamName + ")", aParam.ParamValue);
                    sSQL = SubsToken(sSQL, "@PAR(" + aParam.ParamName + ")", aParam.ParamValue);
                    sSQL = SubsToken(sSQL, "%" + aParam.ParamName + "%", aParam.ParamValue);
                    if (aParam.ParamName == "CURRENT_DATABASE_ID") {  //FLC 29062010: Aplicar CURRENT_DATABASE_ID Dinamico igual que hace DataCycle
                        sIdDatabaseQuery = Convert.ToInt32(aParam.ParamValue);
                    }
                }
                
                sSQL = SubsToken(sSQL, "@FIELD(EXEC_USER_ID)", sIdUser);
                sSQL = SubsToken(sSQL, "@PAR(EXEC_USER_ID)", sIdUser);
                sSQL = SubsToken(sSQL, "%EXEC_USER_ID%", sIdUser);

                //sSQLTemp = "SELECT INTERNALEMAIL FROM OPUSER where IDUSUARIO= " & IdUser

                Int32 IdUser = Convert.ToInt32(sIdUser);

                OPUSER queryDataMail  = db.OPUSER.Where(p => p.IDUSUARIO == IdUser).FirstOrDefault() ;
               // if (queryDataMail != null)
               // {
                    sSQL = SubsToken(sSQL, "@FIELD(EXEC_USER_EMAIL)", queryDataMail.INTERNALEMAIL.ToString());
                    sSQL = SubsToken(sSQL, "@PAR(EXEC_USER_EMAIL)", queryDataMail.INTERNALEMAIL.ToString());
                    sSQL = SubsToken(sSQL, "%EXEC_USER_EMAIL%", queryDataMail.INTERNALEMAIL.ToString());
                //}

                //sSQLTemp = "SELECT * FROM BTPARAMUSER inner join btitemparam on btitemparam.idparam=btparamuser.idparam  WHERE IdItem=" + iDPROJECT.ToString() + " AND iduser= " + sIdUser.ToString();
                IEnumerable<Param> paramsuser = (from m in db.BTPARAMUSER
                                                 join n in db.BTITEMPARAM on m.IdParam equals n.IdParam
                                                 where n.IdItem == iDPROJECT & m.IdUser == IdUser
                                                 select new Param { ParamValue = m.ParamValue, ParamName = n.ParamName });

                if (paramsuser == null) { return sSQL; }
                foreach (Param a in paramsuser)
                {
                    ParamNameTemp = a.ParamName;
                    ParamValueTemp = a.ParamValue;
      
                    sSQL = SubsToken(sSQL, "@FIELD(" + ParamNameTemp + ")", ParamValueTemp);
                    sSQL = SubsToken(sSQL, "@PAR(" + ParamNameTemp + ")", ParamValueTemp);
                    sSQL = SubsToken(sSQL, "%" + ParamNameTemp + "%", ParamValueTemp);
                    if (ParamNameTemp.IndexOf(sIdDatabaseQuery.ToString()) > 0) {     //FLC 29062010: Aplicar CURRENT_DATABASE_ID Dinamico igual que hace DataCycle
                        sIdDatabaseQuery_str = SubsToken(sIdDatabaseQuery.ToString(), "@PAR(" + ParamNameTemp + ")", ParamValueTemp);
                        sIdDatabaseQuery = Convert.ToInt32(sIdDatabaseQuery_str);
                    }
                }

                return sSQL;
            }
            catch {
                //sError = "Error en el método CServices.Preprocess. Causa: " & Err.Number & "-" & Err.Description
                return "";

                throw new NotImplementedException();
            }
        }


        private string SubsToken(String SourceString , String TokenToSubstitute , String newToken )
        {

            int pos;
            String leftPart;
            String CurrentString,rightPart;


            try {
                if (TokenToSubstitute == newToken) {
                    return SourceString;
                } else {

                    CurrentString = SourceString;
                    if (CurrentString.Length > 0) { pos = 1; } else { pos = 0; }

                    while (pos > 0) {
                            pos = CurrentString.ToUpper().IndexOf(TokenToSubstitute.ToUpper(), pos);
                            if (pos > 0)
                            {
                                leftPart = CurrentString.Substring(0, pos - 1);
                                rightPart = CurrentString.Substring(CurrentString.Length - pos - TokenToSubstitute.Length + 1);
                                CurrentString = leftPart + newToken + rightPart;
                                if (newToken.ToUpper().IndexOf(TokenToSubstitute.ToUpper(), 1) > 0)
                                {
                                    pos = 0;
                                }
                            }
                     }

                    return CurrentString;
                    }
            }

            catch {
                return "";
                throw new NotImplementedException(); }
        }


        public int GetFreeID(int a, string e)
        {
            int rid;
            rid = -1;
            try
            {
                MAXIMO maxi = db.MAXIMO.SingleOrDefault(c => c.IDTABLA == a);
                rid = maxi.MAXIMO1.Value + 1;
                db.MAXIMO.Single(c => c.IDTABLA == a).MAXIMO1 = rid;
                db.SaveChanges();

            }

            catch (Exception ex)
            {
                e = ex.Message.ToString();

            }
            return rid;

        }


        public IEnumerable<BTITEMPARAM> ListarParams(Int32 procid, string sParamsFilled = "")
        {

            IEnumerable<BTITEMPARAM> v2 = (from c1 in
                                          ((from c in db.BTITEMPARAM select c).ToList())
                                               // join c2 in db.BTPARAMTYPE on c1.ParamType equals c2.IdParamTypeItem
                                           where (c1.IdItem == procid && c1.SourceType ==1)
                                           select c1).ToList();


            IEnumerable<BTITEMPARAM> Parametrosdelproceso = (from m in v2
                                                             select m).ToList().OrderBy(m => m.PresentationOrder);

            bool tmpnewpanel = false;
            bool newpanel = false;

            //recuperar los valores de cada parametro
            foreach (var param in Parametrosdelproceso)
            {

                if (newpanel) break;

                if (sParamsFilled.IndexOf("|" + param.ParamName + "|") < 0)
                {

                    if ((param.StartNewParamPanel == false) || ((param.StartNewParamPanel == true) & tmpnewpanel == false))
                    {
                        if (tmpnewpanel == false) { tmpnewpanel = true; }

                        var ismulti = (from r in ((from s in db.BTITEMPARAM select s).ToList())
                                       join t in db.BTPARAMTYPE on r.ParamType equals t.IdParamTypeItem
                                       where r.IdParam == param.IdParam
                                       select t.IsMultiValue);

                        var ParProp = (from r in ((from s in db.BTITEMPARAM select s).ToList())
                                       join t in db.BTPARAMTYPE on r.ParamType equals t.IdParamTypeItem
                                       where r.IdParam == param.IdParam
                                       select t );
                        if (Convert.ToInt32(ParProp.SingleOrDefault().IdDomainType) == 1)
                        {
                            ViewData["ParamSize" + Convert.ToString(param.IdParam)] = ParProp.SingleOrDefault().UserInputControlWidth;
                            ViewData[Convert.ToString(param.ParamName)] = param.DefaultValue;

                        }
                        else { 
                            if (ismulti.LastOrDefault() == true) { ViewData["ParamMulti" + Convert.ToString(param.IdParam)] = 1; }
                            else
                            {
                                ViewData["ParamMulti" + Convert.ToString(param.IdParam)] = 0;
                            }
                        }


                        if (param.StartNewParamPanel == true) { ViewData["ParamNewPanel" + Convert.ToString(param.IdParam)] = 1; }
                        else
                        {
                            ViewData["ParamNewPanel" + Convert.ToString(param.IdParam)] = 0;
                        }

                        var dbQueryId = (from r in ((from s in db.BTITEMPARAM select s).ToList())
                                         join t in db.BTPARAMTYPE on r.ParamType equals t.IdParamTypeItem
                                         where r.IdParam == param.IdParam & t.IdDomainType == 3
                                         select t.IdDomainQueryCall);

                        if (dbQueryId.LastOrDefault() > 0)
                        { ViewData[Convert.ToString(param.ParamName)] = GetParamValuesFromDB(dbQueryId.FirstOrDefault().Value); }
                        else

                        {
                            if (Convert.ToInt32(ParProp.SingleOrDefault().IdDomainType)>1) {
                                ViewData[Convert.ToString(param.ParamName)] = GetParamValues(param.IdParam); }
                        }

                    }
                    else
                    {
                        newpanel = true;
                    }
                }
                else
                {

                    int ParamPos = sParamsFilled.IndexOf("|" + param.ParamName + "|") + param.ParamName.Length + 2;

                    //int lParam = sParamsFilled.Length - (param.ParamName.Length + 2);
                    string tmpParamValue = sParamsFilled.Substring(ParamPos);
                    string ParamValue = "";
                    //ParamValue = Left(tmpParamValue, instr(1, tmpParamValue, "|") - 1)
                    if (tmpParamValue.IndexOf("|", 0) > 0)
                    {
                        ParamValue = tmpParamValue.Substring(0, tmpParamValue.IndexOf("|", 1));
                    }
                    ViewData[Convert.ToString(param.ParamName)] = ParamValue;
                }
            }
            return Parametrosdelproceso.AsEnumerable();
        }


        public ActionResult Index(int procid)
        {
            ViewBag.IDProcess = procid;
            var nombreproceso = db.BTITEM.Where(p => p.ID == procid).FirstOrDefault().LOGICAL_NAME;
            ViewData["NombreProceso"] = nombreproceso;
            return View(ListarParams(procid));

        }

        public ActionResult RunPParams(int procid, string sParamsFilled = "")
        {
            try
            {
                ViewBag.IDProcess = procid;
                ViewBag.IDFolder = db.BTSUBJECT_ITEMS.Where(p => p.IdItem == procid).FirstOrDefault().IdSubject;
                var nombreproceso = db.BTITEM.Where(p => p.ID == procid).FirstOrDefault().LOGICAL_NAME;
                ViewData["NombreProceso"] = nombreproceso;

                return View(ListarParams(procid));
            }
            catch { return View("Error"); }

        }


        [HttpPost]
        public ActionResult RunPParams(int procid, FormCollection form)
        {
            System.Collections.ArrayList sParamsNames = new System.Collections.ArrayList();
            System.Collections.ArrayList sParamsValues = new System.Collections.ArrayList();

            var sParamsFilled = "|";

            for (int i = 0; i < form.Keys.Count; i++)
            {
                sParamsNames.Add(form.Keys[i].ToString());
                sParamsValues.Add(form[i].ToString());
                if (sParamsNames[i].ToString() != "NewPanel")
                {
                    sParamsFilled = sParamsFilled + sParamsNames[i].ToString() + '|' + sParamsValues[i].ToString() + '|';
                }

            }
            var count = form.Count;
            int k = 0;
            bool IsNewPanel = false;
            while (k < count && !IsNewPanel)
            {
                var KeyName = form.GetKey(k);
                k++;
                if (KeyName == "NewPanel") { IsNewPanel = true; }
            }

            if (IsNewPanel && form["NewPanel"].ToString() == "IsNewPanel")
            {
                ViewData["Panel2"] = 1;
                return View(ListarParams(procid, sParamsFilled));
            }
            else
            {

                return RedirectToAction("RunProcWParams", new { sParams = sParamsFilled, id = procid });
            }


        }

        public ActionResult RunProcWParams(string sParams, int id = 0)
        {

            string sMachineName = "PC"; //GetThisMachineName()
            string sError = "";
            //Demanem un nou ID per al job
            int newJobID = GetFreeID(7, sError);
            if (sError != "")
            {
                return RedirectToAction("Index");
            };
            string sIdUser = AccountController.GetUsrId(User).ToString();
            string sParamString = sParams;
            Int32 JobWaiting = 0;
            string TEST_ASAP = "TEST_PROCESS_ASAP";

            string ssql = "INSERT INTO OPJOB(idJob , iduser, idServer, idPlan, IDProcess, idParentJob, Rootjob, Depth, EXECUTIONSTATUS, Trace_Level, UserParams)";
            ssql = ssql + "VALUES (" + newJobID + "," + sIdUser + ",0, 0, " + id + ",0," + id + ",0, " + JobWaiting.ToString() + ",0, '" + sParamString.Replace("'", "''") + "') ";

            int newMessageID = GetFreeID(3, "");
            //'Planifiquem el proces ASAP
            string sInsertMessageSQL = "INSERT INTO COMUNICATOR (IDMessage, Moment, IDServer, Command, Param1, Param2, Param3, Priority, OriginIdUser, OriginMachineName, ServerLock)";
            sInsertMessageSQL = sInsertMessageSQL + "VALUES (" + newMessageID + ",GetDate(),0,'" + TEST_ASAP + "','" + id + "','" + newJobID + "','" + sParamString.Replace("'", "''") + "',0," + sIdUser + ",'" + sMachineName + "',0)";

            db.OPJOB.SqlQuery(ssql);
            db.Database.ExecuteSqlCommand(ssql);
            db.SaveChanges();
            db.COMUNICATOR.SqlQuery(sInsertMessageSQL);
            db.Database.ExecuteSqlCommand(sInsertMessageSQL);
            db.SaveChanges();

            ViewBag.IDP = id;
            ViewBag.IDJ = newJobID;

            IEnumerable<OPJOB> jobrunning = (from c1 in
                                          ((from c in db.OPJOB select c).ToList())
                                             where (c1.IDJOB == newJobID)
                                             select c1).ToList();
            if (jobrunning == null)
            {
                return HttpNotFound();
            }
            return View(jobrunning);

            //return View(GetLastJobStatus(newJobID));

        }

        [OutputCache(NoStore = true, Location = System.Web.UI.OutputCacheLocation.Client, Duration = 1)]
        public ActionResult GetLastJobStatus(Int32 idjob)
        {

            IEnumerable<OPJOB> v2 = from jobs in db.OPJOB where jobs.IDJOB == idjob select jobs;
            return PartialView("VistaEstadoJob", v2.FirstOrDefault());

        }


        //
        // GET: /Params/Details/5

        public ActionResult Details(int id = 0)
        {
            BTITEMPARAM btitemparam = db.BTITEMPARAM.Find(id);
            if (btitemparam == null)
            {
                return HttpNotFound();
            }
            return View(btitemparam);
        }

        //
        // GET: /Params/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Params/Create

        [HttpPost]
        public ActionResult Create(BTITEMPARAM btitemparam)
        {
            if (ModelState.IsValid)
            {
                db.BTITEMPARAM.Add(btitemparam);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(btitemparam);
        }

        //
        // GET: /Params/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BTITEMPARAM btitemparam = db.BTITEMPARAM.Find(id);
            if (btitemparam == null)
            {
                return HttpNotFound();
            }
            return View(btitemparam);
        }

        //
        // POST: /Params/Edit/5

        [HttpPost]
        public ActionResult Edit(BTITEMPARAM btitemparam)
        {
            if (ModelState.IsValid)
            {
                db.Entry(btitemparam).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(btitemparam);
        }

        //
        // GET: /Params/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BTITEMPARAM btitemparam = db.BTITEMPARAM.Find(id);
            if (btitemparam == null)
            {
                return HttpNotFound();
            }
            return View(btitemparam);
        }


        //
        // POST: /Params/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BTITEMPARAM btitemparam = db.BTITEMPARAM.Find(id);
            db.BTITEMPARAM.Remove(btitemparam);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public class BTITEMPARAMName
        {
        }

        public Collection<Param> DecompressParam(String sParamString, ref string sError)
        {

            String[] sArray;
            Int32 n = 0;
            Collection<Param> ParamSet = new Collection<Param>();
            //Param oParam;

            try
            {
                sArray = sParamString.Split('|');

                for (n = 1; n < sArray.Length; n = +2)
                {
                    Param oParam = new Param();
                    oParam.ParamName = sArray[n];
                    oParam.ParamValue = sArray[n + 1];
                    ParamSet.Add(oParam);
                    oParam = null;
                }

                return ParamSet;
                
            }
            catch (Exception ex)
            {

                sError = "Error en el método CServices.DecompressParam. Causa: " + ex + "-" + ex.Message;
                return ParamSet;
                
            }
            
        }


        public void DeclareParam(String ParamName, String ParamValue, ref System.Collections.ObjectModel.Collection<Param> ParamSet) {
            try {

                MvcApplication1.Models.Param oParam;
                oParam = new MvcApplication1.Models.Param();
                oParam.ParamName = ParamName.ToString();
                oParam.ParamValue = ParamValue;
                ParamSet.Add(oParam);
                oParam = null;

            }
            catch (Exception ex) { }

        }


          double ConvertDateToJulian(DateTime  fecha) { 
            
                return fecha.ToOADate() + 2415018.5;

            }

        

        private void DefineTimeContextParams(DateTime moment, ref System.Collections.ObjectModel.Collection<Param> ParamSet) {

            try
            {
                
                this.DeclareParam("CURRENT_HOUR[NUMBER]", String.Format("{hhmmss}", moment),ref ParamSet);
                this.DeclareParam("CURRENT_HOUR[HH:MM:SS]", String.Format("{hh:mm:ss}", moment), ref ParamSet);
                this.DeclareParam("CURRENT_DATE[NUMBER]", String.Format("{yyyymmdd}", moment),ref ParamSet); //Format(moment, "yyyymmdd"), ParamSet
                this.DeclareParam("CURRENT_DATE[JULIAN]", ConvertDateToJulian(moment).ToString(), ref ParamSet);
                this.DeclareParam("CURRENT_DATE[DD-MM-YYYY]", String.Format("{dd-mm-yyyy}", moment), ref ParamSet);

                this.DeclareParam("CURRENT_DATE[D]", String.Format("{d}", moment), ref ParamSet);
                this.DeclareParam("CURRENT_DATE[DD]", String.Format("{dd}", moment), ref ParamSet);

                this.DeclareParam("CURRENT_MONTH[M]", String.Format("{m}", moment), ref ParamSet);
                this.DeclareParam("CURRENT_MONTH[MM]", String.Format("{mm}", moment), ref ParamSet);
                this.DeclareParam("CURRENT_MONTH[YYYY]", String.Format("{yyyy}", moment), ref ParamSet);
                this.DeclareParam("CURRENT_MONTH[YY]", String.Format("{yy}", moment), ref ParamSet);
                
                var culture = new System.Globalization.CultureInfo("es-ES");
                var day = culture.DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek);
                var month = culture.DateTimeFormat.GetMonthName(DateTime.Today.Month);
                this.DeclareParam("CURRENT_DATE_WEEKDAY_NAME[ESP]",day, ref ParamSet);
                this.DeclareParam("CURRENT_MONTH_NAME[ESP]", month, ref ParamSet);

                culture = new System.Globalization.CultureInfo("en-EN");
                day = culture.DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek);
                month = culture.DateTimeFormat.GetMonthName(DateTime.Today.Month);
                this.DeclareParam("CURRENT_DATE_WEEKDAY_NAME[ENG]", day, ref ParamSet);
                this.DeclareParam("CURRENT_MONTH_NAME[ENG]", month, ref ParamSet);
                
                culture = new System.Globalization.CultureInfo("ca-ES");
                day = culture.DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek);
                month = culture.DateTimeFormat.GetMonthName(DateTime.Today.Month);
                this.DeclareParam("CURRENT_DATE_WEEKDAY_NAME[CAT]", day, ref ParamSet);
                this.DeclareParam("CURRENT_MONTH_NAME[CAT]", month, ref ParamSet);

                this.DeclareParam("YESTERDAY_DATE[NUMBER]", String.Format("{yyyymmdd}", DateTime.Now.AddDays(-1)), ref ParamSet);
                this.DeclareParam("YESTERDAY_DATE[DD-MM-YYYY]", String.Format("{dd-mm-yyyy}", DateTime.Now.AddDays(-1)), ref ParamSet);
                this.DeclareParam("YESTERDAY_DATE[JULIAN]", ConvertDateToJulian(DateTime.Now.AddDays(-1)).ToString(), ref ParamSet);
                
                var firstDayOfMonth = new DateTime(moment.Year, moment.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                
                this.DeclareParam("CURRENT_MONTH_FIRST_DATE[NUMBER]", String.Format("{yyyymmdd}", firstDayOfMonth), ref ParamSet);
                this.DeclareParam("CURRENT_MONTH_FIRST_DATE[DD-MM-YYYY]", String.Format("{dd-mm-yyyy}", firstDayOfMonth), ref ParamSet);
                this.DeclareParam("CURRENT_MONTH_FIRST_DATE[JULIAN]", ConvertDateToJulian(firstDayOfMonth).ToString(), ref ParamSet);

                this.DeclareParam("CURRENT_MONTH_LAST_DATE[NUMBER]", String.Format("{yyyymmdd}", lastDayOfMonth), ref ParamSet);
                this.DeclareParam("CURRENT_MONTH_LAST_DATE[DD-MM-YYYY]", String.Format("{dd-mm-yyyy}", lastDayOfMonth), ref ParamSet);
                this.DeclareParam("CURRENT_MONTH_LAST_DATE[JULIAN]", ConvertDateToJulian(lastDayOfMonth).ToString(), ref ParamSet);
                        
                //DeclareParam "PREVIOUS_YEAR[YYYY]", Int(Format(moment, "yyyy")) - 1, ParamSet
                //DeclareParam "PREVIOUS_YEAR[YY]", Right(Str(Int(Format(moment, "yy")) - 1), 2), ParamSet

                //DeclareParam "PREVIOUS_MONTH_NUMBER[M]", Format(PreviousMonthFirstDate(moment), "m"), ParamSet
                //DeclareParam "PREVIOUS_MONTH_NUMBER[MM]", Format(PreviousMonthFirstDate(moment), "mm"), ParamSet
                //DeclareParam "YEAR_OF_PREVIOUS_MONTH[YY]", Format(PreviousMonthFirstDate(moment), "yy"), ParamSet
                //DeclareParam "YEAR_OF_PREVIOUS_MONTH[YYYY]", Format(PreviousMonthFirstDate(moment), "yyyy"), ParamSet

                //DeclareParam "PREVIOUS_MONTH_NAME[ESP]", NombreMes(PreviousMonthNumber(moment), "ESP"), ParamSet
                //DeclareParam "PREVIOUS_MONTH_NAME[ENG]", NombreMes(PreviousMonthNumber(moment), "ENG"), ParamSet
                //DeclareParam "PREVIOUS_MONTH_NAME[CAT]", NombreMes(PreviousMonthNumber(moment), "CAT"), ParamSet

                //DeclareParam "PREVIOUS_MONTH_FIRST_DATE[NUMBER]", Format(PreviousMonthFirstDate(moment), "yyyymmdd"), ParamSet
                //DeclareParam "PREVIOUS_MONTH_FIRST_DATE[JULIAN]", ConvertDateToJulian(PreviousMonthFirstDate(moment)), ParamSet
                //DeclareParam "PREVIOUS_MONTH_FIRST_DATE[DD-MM-YYYY]", Format(PreviousMonthFirstDate(moment), "dd-mm-yyyy"), ParamSet

                //DeclareParam "PREVIOUS_MONTH_LAST_DATE[NUMBER]", Format(PreviousMonthLastDate(moment), "yyyymmdd"), ParamSet
                //DeclareParam "PREVIOUS_MONTH_LAST_DATE[JULIAN]", ConvertDateToJulian(PreviousMonthLastDate(moment)), ParamSet
                //DeclareParam "PREVIOUS_MONTH_LAST_DATE[DD-MM-YYYY]", Format(PreviousMonthLastDate(moment), "dd-mm-yyyy"), ParamSet

            }
            catch { }

}

        

        public bool LoadParamsOfItemFromDB(int iditem, int idQueryCall,Collection<Param> ParamSet) {
            String  sError;            
            try {
                
                //Dim aParam As CParam

                IEnumerable<BTITEMPARAM> v1 = (from c1 in
                                              ((from c in db.BTITEMPARAM select c).ToList())
                                              where (c1.IdItem == iditem)
                                              select c1).ToList();

                IEnumerable<BTQUERYCALL_PARAMVALUES> v2 = (from m in v1
                                             join t in db.BTQUERYCALL_PARAMVALUES on m.IdParam equals t.IdParam
                                             where (t.IdQueryCall == idQueryCall)
                                             select t).ToList();

                if (v1.Count() == 0) { return false; }

                foreach (BTITEMPARAM val1 in v1)
                {
                    foreach (BTQUERYCALL_PARAMVALUES val2 in v2)
                    {
                        Param oParam = new Param();

                        oParam.ParamName = val1.ParamName.ToString();
                        oParam.ParamValue= val2.ParamValue.ToString();
                        ParamSet.Add(oParam);

                    }
                }
                
            return true;

    } catch (Exception ex) {
       
        sError = "Error en el método CServices.LoadParamsOfItemFromDB. Causa: " + ex.Message;
         return false;

         }
        }

    }
}