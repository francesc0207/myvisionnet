﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{   
    //[Authorize(Users=@"APENET\Administrador")]   // En caso de uso de Aut.Windows
    public class PrjuserController : Controller
    {
        
        private DCREntities db = new DCREntities();

        //
        // GET: /Prjuser/

        public IEnumerable<RPPROJECTSUSER> Py_Listar(string sGroupString)
        {

            sGroupString = "10,25,17";
            var myList = new List<string>(sGroupString.Split(','));
            IEnumerable<int> ListIDs = myList.Select(int.Parse).ToList();
            var usrs = from k in ListIDs select new RPPROJECTSUSER { IDUSER  = k };

            IEnumerable<RPPROJECTSUSER> v3 = (from c1 in
                     ((from c in db.RPPROJECTSUSER select c).ToList())
                      join c2 in usrs  
                      on c1.IDUSER 
                      equals c2.IDUSER 
                      select c1).ToList();

            IEnumerable<RPPROJECTSUSER> projs = (from m in v3
                                            where (m.TYPE == 5 )
                                            select new  RPPROJECTSUSER  {ID = m.ID  , LOGICAL_NAME  = m.LOGICAL_NAME, IDUSER  =m.IDUSER }) ;

            IEnumerable<RPPROJECTSUSER> projsG = from j in projs group j by j.LOGICAL_NAME into g select new RPPROJECTSUSER { LOGICAL_NAME = g.Key, ID = g.Max(d => d.ID) };
            return projsG;
        }

        public ActionResult Proyectos()
        {

            string sGroupString = "";
            sGroupString = AccountController.GetPerm(User);

            if (sGroupString != "La secuencia no contiene elementos")
            {
                ViewBag.NC = Properties.Settings.Default.ColumnsNum;

                IEnumerable<BTPROJECT> projects = (from j in db.BTPROJECT select j).ToList();
                return View(projects);
                //return View(Py_Listar(sGroupString));
            }
            else
            {
                return View("El usuario no tiene proyectos asignados");
            };
        }




        public ActionResult Index()
        {
        
            string sGroupString = "";
            sGroupString = AccountController.GetPerm(User) ;

           if (sGroupString !="La secuencia no contiene elementos") {
            return View(Py_Listar(sGroupString));} 
           else {
            return View("El usuario no tiene proyectos asignados");
            };
        }

        //
        // GET: /Prjuser/Details/5

        public ActionResult Details(Int32 id) 
        
        {
            
            RPPROJECTSUSER rpprojectsuser = db.RPPROJECTSUSER.Where(u => u.ID.Equals(id)).SingleOrDefault();
            if (rpprojectsuser == null)
            {
                return HttpNotFound();
            }
            return View(rpprojectsuser);
        }

        //
        // GET: /Prjuser/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Prjuser/Create

        [HttpPost]
        public ActionResult Create(RPPROJECTSUSER rpprojectsuser)
        {
            if (ModelState.IsValid)
            {
                db.RPPROJECTSUSER.Add(rpprojectsuser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rpprojectsuser);
        }

        //
        // GET: /Prjuser/Edit/5

        public ActionResult Edit(int id = 0)
 
        {
            BTPROJECT rpprojectsuser = db.BTPROJECT.Where(u => u.IDPROJECT.Equals(id)).SingleOrDefault();
            if (rpprojectsuser == null)
            {
                return HttpNotFound();
            }
            return View(rpprojectsuser);
        }

        //
        // POST: /Prjuser/Edit/5

        [HttpPost]
        public ActionResult Edit(BTPROJECT rpprojectsuser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rpprojectsuser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rpprojectsuser);
        }

        //
        // GET: /Prjuser/Delete/5

        public ActionResult Delete(int id = 0)
        {
            RPPROJECTSUSER rpprojectsuser = db.RPPROJECTSUSER.Find(id);
            if (rpprojectsuser == null)
            {
                return HttpNotFound();
            }
            return View(rpprojectsuser);
        }

        //
        // POST: /Prjuser/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            RPPROJECTSUSER rpprojectsuser = db.RPPROJECTSUSER.Find(id);
            db.RPPROJECTSUSER.Remove(rpprojectsuser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}