﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    public class ReportsController : Controller
    {
        private DCREntities db = new DCREntities();

        //
        // GET: /Reports/

        public ActionResult Index(int idp)

        {
            
    //sSQL = "SELECT max(BTITEMUSER.ViewResultsMode) as ViewResultsMode FROM BTITEMUSER WHERE IdItem = " & sIdProcess & " AND IDUSER IN (" & sGroupString & ")"
    //sViewResultModeXML = GetXMLData(sSQL, sError)
    //Set rsViewResultsMode = InternalRecordsetFromXMLString(sViewResultModeXML, adLockReadOnly, sError)
    //If Not rsViewResultsMode Is Nothing Then
    //    sViewResultsMode = IIf(IsNull(rsViewResultsMode.Fields("ViewResultsMode")), "", rsViewResultsMode.Fields("ViewResultsMode"))
        
    //    Select Case sViewResultsMode
    //        Case "1", "2":
    //            'Obtenim els reports de l'ultim job que tenen el nom y directori de generació diferents.
    //            'S'ha definit funció nova perquè ara BTREPORT té tots els reports on logicalfolder són diferents, aleshores tenim registres de + d'una
    //            'execució
    //            sSQL = "SELECT DISTINCT btreport.* from BTREPORT,"
    //            sSQL = sSQL & "(SELECT distinct logicalname,lastfilepath,max(lastidjob) as UltimoJob "
    //            sSQL = sSQL & " FROM BTREPORT INNER JOIN BTITEMUSER ON (BTITEMUSER.IdItem=BTReport.idProcess) WHERE"
    //            sSQL = sSQL & " ((BTITEMUSER.ViewResultsMode=1) OR (BTITEMUSER.ViewResultsMode=2 AND BTREPORT.IdVirtualExecutor=" & sUserId & ")) AND "
    //            sSQL = sSQL & "(BTREPORT.IdProcess =" & sIdProcess & ") AND (BTITEMUSER.IdUser IN (" & sGroupString & "))"
    //            sSQL = sSQL & " AND (BTREPORT.IsPublished=1)"
    //            sSQL = sSQL & " GROUP BY logicalname,lastfilepath) as t"
    //            sSQL = sSQL & " WHERE btreport.LogicalName = t.LogicalName And btreport.LastFilePath = t.LastFilePath And btreport.lastidjob = t.UltimoJob"
    //        Case "3":
    //            sSQL = "SELECT DISTINCT btreport.* from BTREPORT,"
    //            sSQL = sSQL & "(SELECT distinct logicalname,lastfilepath,max(lastidjob) as UltimoJob "
    //            sSQL = sSQL & " FROM BTREPORT INNER JOIN BTITEMUSER ON (BTITEMUSER.IdItem=BTReport.idProcess) WHERE"
    //            sSQL = sSQL & " (BTITEMUSER.ViewResultsMode=3) AND (BTREPORT.IdProcess =" & sIdProcess & ") AND (BTITEMUSER.IdUser IN (" & sGroupString & "))"
    //            sSQL = sSQL & " AND (BTREPORT.IsPublished=1)"
    //            sSQL = sSQL & " GROUP BY logicalname,lastfilepath) as t"
    //            sSQL = sSQL & " WHERE btreport.LogicalName = t.LogicalName And btreport.LastFilePath = t.LastFilePath And btreport.lastidjob = t.UltimoJob"
    //        Case Else:
    //            sError = "Nivel de seguridad desconocido"
    //    End Select
    //End If
    //sSQL = sSQL & " ORDER BY btreport.LastdateGen" 'FLC 20090701 - Cambios en la ordenación

            var myList = new List<string>(AccountController.GetPerm(User).Split(','));
            IEnumerable<int> ListIDs = myList.Select(int.Parse).ToList();
            var usrs = from k in ListIDs select new OPUSER { IDUSUARIO = k };

     
            IEnumerable<BTITEMUSER> v2 = (from c1 in
                                       ((from c in usrs select c).ToList())
                                   join c2 in db.BTITEMUSER
                                   on c1.IDUSUARIO 
                                   equals c2.IDUSER
                                   where (c2.IDITEM == idp)
                                   select c2).ToList() ;

            var vr = v2.Select(obj => obj.ViewResultsMode ).Max(); 

            ViewData["idproc"] =idp;
            var nombreproceso = db.BTITEM.Where(p => p.ID == idp).FirstOrDefault().LOGICAL_NAME;
            ViewData["nomproc"] = nombreproceso;

            switch (vr) 
	            {
	                case 1: case 2:  // es 1 o 2
	                {
                        IEnumerable<BTREPORT> v3 = (from c1 in
                                                     ((from c in v2 select c).ToList())
                                                      join c2 in db.BTREPORT
                                                      on c1.IDITEM 
                                                      equals c2.IdProcess
                                                    where ((c1.ViewResultsMode == 1) | (c1.ViewResultsMode == 2 & c2.IdVirtualExecutor == AccountController.GetUsrId(User))) & c2.IdProcess == idp & c2.IsPublished == true 
                                                      select c2).ToList().Distinct();
                        return View(v3.ToList().OrderByDescending(a => a.LastDateGen)) ;
		            }
	                case 3:

                     IEnumerable<BTREPORT> v4 = (from c1 in
                                                     ((from c in v2 select c).ToList())
                                                      join c2 in db.BTREPORT
                                                      on c1.IDITEM 
                                                      equals c2.IdProcess  
                                                      where c1.ViewResultsMode==3 & c2.IdProcess ==idp & c2.IsPublished ==true 
                                                      select c2).ToList().Distinct();
                        return View(v4.ToList().OrderByDescending(a => a.LastDateGen)) ;

	                default:
                    // You can use the default case.

                    
		            return View(db.BTREPORT.ToList().OrderByDescending(s => s.LastDateGen).Select(b=>b.IdProcess <0 ));
	            }
       
            //return View();
        }

        //
        // GET: /Reports/Details/5

        public ActionResult Details(int idp = 0)
        {
            BTREPORT btreport = db.BTREPORT.Find(idp);
            if (btreport == null)
            {
                return HttpNotFound();
            }
            return View(btreport);
        }

        //
        // GET: /Reports/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Reports/Create

        [HttpPost]
        public ActionResult Create(BTREPORT btreport)
        {
            if (ModelState.IsValid)
            {
                db.BTREPORT.Add(btreport);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(btreport);
        }

        //
        // GET: /Reports/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BTREPORT btreport = db.BTREPORT.Find(id);
            if (btreport == null)
            {
                return HttpNotFound();
            }
            return View(btreport);
        }

        //
        // POST: /Reports/Edit/5

        [HttpPost]
        public ActionResult Edit(BTREPORT btreport)
        {
            if (ModelState.IsValid)
            {
                db.Entry(btreport).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(btreport);
        }

        //
        

        public ActionResult Delete(int id = 0)
        {
            BTREPORT btreport = db.BTREPORT.Find(id);
            if (btreport == null)
            {
                return HttpNotFound();
            }
            return View(btreport);
        }

        //public ActionResult VistaReport(Int32 idrp)
        //{
        //    return GetReport(idrp);
        //}

        public bool RefReport()
        {
            return true;
        }


        public ActionResult GetReport(Int32 idrepo)
        {
            BTREPORT btreport = db.BTREPORT.Find(idrepo);
            
            if (btreport == null)
            {
                return PartialView("VistaNoReport"); 
            }
            
            return PartialView("VistaReport2", btreport );
        }

        
        public FileStreamResult DownloadFile(string fichero,string ruta)
        {
           
             
                string rutaf = ruta + "\\" + fichero; ;
                if (System.IO.File.Exists(rutaf))
                {
                    byte[] fileBytes = System.IO.File.ReadAllBytes(rutaf);
                    string fileName = rutaf.Substring(rutaf.LastIndexOf("\\"));

                    System.IO.FileStream fs = new System.IO.FileStream(rutaf, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    return File(fs, System.Net.Mime.MediaTypeNames.Application.Pdf);
                }
                else
                {
                return null;
            }

           
        
    }


        
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BTREPORT btreport = db.BTREPORT.Find(id);
            db.BTREPORT.Remove(btreport);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
          }
    }
}