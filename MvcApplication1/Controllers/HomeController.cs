﻿using MvcApplication1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Bienvenido a DataCycle MyReporterAir";

            return View();
        }

        public ActionResult Contact2()
        {
            IEnumerable<Model> list = Repository.GetData();

            return View(list);
        }

        [OutputCache(NoStore = true, Location = System.Web.UI.OutputCacheLocation.Client, Duration = 3)] // every 3 sec
        public ActionResult GetContributor()
        {
            IEnumerable<Model> list = Repository.GetData();

            int min = list.Min(m => m.ID);
            int max = list.Max(m => m.ID);

            int randomId = new Random().Next(min, (max + 1));

            Model model = list.FirstOrDefault(m => m.ID == randomId);
            return PartialView("Contributor", model);

        }
        private DCREntities db = new DCREntities();

        [OutputCache(NoStore = true, Location = System.Web.UI.OutputCacheLocation.Client, Duration = 1)]
        public ActionResult GetLastJobStatus(Int32 idjob)
        {

            IEnumerable<OPJOB> v2 = (from c1 in
                                          ((from c in db.OPJOB select c).ToList())

                                     where (c1.IDJOB == idjob)
                                     select c1).ToList();
            //return v2.AsEnumerable();

            return PartialView("VistaEstadoJob", v2.AsEnumerable().FirstOrDefault());

        }
        public ActionResult About()
        {
            ViewBag.Message = "Ejecute los reports en cualquier momento o planifique para que se lancen más tarde";
            ViewData["UsrAdmin"] = "sa";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Página de contacto.";

            return View();
        }
        

        public PartialViewResult Germany()
        {
            var result = from r in db.BTITEM
                         where r.ID == 183
                         select r;
            return PartialView("_Country", result);
        }

        public PartialViewResult Mexico()
        {
            var result = from r in db.BTITEM
                         where r.ID ==134
                         select r;
            return PartialView("_Country", result);
        }


    }
}
