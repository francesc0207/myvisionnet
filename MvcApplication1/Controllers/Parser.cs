﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace MvcApplication1.Controllers
{

    public enum eBlockType
    {fieldExpr,     fromExpr,     tableExpr,
     whereExpr,     groupByExpr,     havingExpr,    orderByExpr,
    }
   
    public class Parser

    {

      public  bool  Parse(String sql, string sSelect, Collection<string> Campo, string sFrom, ref Collection<string> Tabla, String sWhere, String sHaving, String sGroupBy, String sOrderBy, String sError)
        { 
        eBlockType mi_step;
         try {
                if (sql == "") {
                    sError = "Se ha intentado analizar una sentencia vacia";
                    return false;
                }
                sql = sql.Trim();
                sql = processSelectSufix(sql, sSelect);


                if (!FirstTokenIs("select", sql, sql, sError)) {
                    return false;
                }

                mi_step = eBlockType.fieldExpr;

                sql = ProcessList(sql, "from", Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy, sError);
                if (!FirstTokenIs("from", sql, sql, sError))
                {
                    return false;
                }
                mi_step = eBlockType.tableExpr;

                if (nextTokenIs("where", sql)) {
                    if (!FirstTokenIs("where", sql, sql, sError)) {
                        return false;
                    }
                    mi_step = eBlockType.whereExpr;

                    if (!ProcessTextBlock(sql, Array("group by", "order by", "having"), sql, Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy, sError)) {
                        return false;
                    }
                }

                if (nextTokenIs("group by", sql))
                {
                    mi_step = eBlockType.groupByExpr;

                    if (!ProcessTextBlock(sql, Array("order by", "having"), sql, Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy, sError))
                    {
                        return false;
                    }
                }
                if (nextTokenIs("having", sql))
                {
                    mi_step = eBlockType.havingExpr;

                    if (!ProcessTextBlock(sql, Array("group by", "order by"), sql, Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy, sError))
                    {
                        return false;
                    }
                }

                if (nextTokenIs("order by", sql))
                {
                    mi_step = eBlockType.orderByExpr;

                    if (!ProcessTextBlock(sql, Array("group by", "having"), sql, Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy, sError))
                    {
                        return false;
                    }
                }
                return true;
                
            }
            catch(Exception ex) {
                sError = "Error en el método CSQLParser.parse. Causa: " + ex.InnerException.ToString() + "-" + ex.Message.ToString();
                return false;

            }
          }

        bool FirstTokenIs(String sToken, String sToParse, ref String sSqlRestante  , ref string sError ) {

            String cleanToken;
            String cleanToParse;
            Int32 posToken = 0;//era long

            cleanToParse = CleanBadChars(sToParse);

            if (cleanToParse.Length < sToken.Length) {
                sError = "Se esperaba un elemento '" + sToken + "' al inicio de la cadena " + sToParse;
                return false;
            }

            if (sToken.ToUpper() == cleanToParse.Substring(0, sToken.Length).ToUpper() ) {

                posToken = sToParse.ToUpper().IndexOf(sToken.ToUpper());
                sSqlRestante = sToParse.Substring(posToken + sToken.Length + 1);

                 return true;
            }
               else
     {

                sError = "Se esperaba un elemento '" + sToken + "' al inicio de la cadena " + sToParse;
                return false;
            }
}

        private string CleanBadChars(string sToParse)
        {
            throw new NotImplementedException();
        }
    }
}