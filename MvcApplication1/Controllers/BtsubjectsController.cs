﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    public class BtsubjectsController : Controller
    {
        private DCREntities db = new DCREntities();

        //
        // GET: /Btsubjects/

        public ActionResult Index(Int32 idprj)
        {
            return View(db.BTSUBJECT.ToList());
        }

        public IEnumerable<Carpeta> Carpetas_Listar(Int32 idprj, string sortOrder ) 
            {
             IEnumerable<Carpeta> subjs = (from m in db.BTSUBJECT
                        join n in db.BTITEMUSER on m.IdSubject equals n.IDITEM 
                        join p in db.RPPROJECTITEM on n.IDITEM equals p.ID 
                        join b in db.BTITEM on m.IdSubject equals b.ID  
                        where n.IDUSER == 17 &  p.IDPROJECT == idprj  & m.IdSubjectParent ==0 & m.SubjectType==2   //Cambiar 17 por User logged 
                       select new Carpeta {Id= m.IdSubject, IdParent= m.IdSubjectParent.Value ,Nombre =b.LOGICAL_NAME });
            

            switch (sortOrder)
            {
                case "_asc":
                    subjs = subjs.OrderBy(s => s.Nombre);
                    break;
                case "_desc":
                    subjs = subjs.OrderByDescending(s => s.Nombre);
                    break;
                default:
                    subjs = subjs.OrderBy(s => s.Nombre);
                    break;
            }


            foreach (Carpeta cp in subjs)
            {
                
                Byte[] imagen_stream = new Byte[0];
                string pathImgs = "/Images/";
                            

                DCREntities db1 = new DCREntities();
                BTIMAGES imgg = db1.BTIMAGES.Find(cp.Id);
                                                
               
                if (imgg !=null)
                {
                    imagen_stream = db1.BTIMAGES.Where(p => p.IdItem == cp.Id).SingleOrDefault().Picture;
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(imagen_stream);
                    //write to file

                    //pathImgs = pathImgs + "Img_" + cp.Id.ToString() + "_folder.jpg";
                    pathImgs = Request.MapPath("~/Images/") + "Img_" + cp.Id.ToString() + "_folder.jpg"; 

                    System.IO.FileStream file = new System.IO.FileStream(pathImgs, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                    ms.WriteTo(file);
                    file.Close();
                    ms.Close();
                }
                else { pathImgs = pathImgs + "folder.jpg"; }
                ViewData[Convert.ToString(cp.Id)] = System.IO.Path.GetFileName(pathImgs);

            }

            return subjs.AsEnumerable();

            }

        public ActionResult Carpetas(Int32 idprj,string sortOrder)

        {
            ViewBag.prj = idprj;
            ViewBag.SortParm = String.IsNullOrEmpty(sortOrder) ? "_desc" : "";
            return View(Carpetas_Listar(idprj, sortOrder));
        }

        public IEnumerable<Carpeta> SubCarpetas_Listar(Int32 idfolder)
        {
            IEnumerable<Carpeta> subjs = (from m in db.BTSUBJECT
                                          join n in db.BTITEMUSER on m.IdSubject equals n.IDITEM
                                          join p in db.RPPROJECTITEM on n.IDITEM equals p.ID
                                          join b in db.BTITEM on m.IdSubject equals b.ID
                                          where n.IDUSER == 17 &  m.IdSubjectParent == idfolder & m.SubjectType == 2
                                          select new Carpeta { Id = m.IdSubject, Nombre = b.LOGICAL_NAME,IdParent=idfolder }).Union (
        //Procesos
                from m in db.BTSUBJECT_ITEMS
                join n in db.BTITEMUSER on m.IdItem equals n.IDITEM
                join p in db.RPPROJECTITEM on n.IDITEM equals p.ID
                join b in db.BTITEM on m.IdItem equals b.ID
                where n.IDUSER == 17 & m.IdSubject == idfolder & b.TYPE == 6   //Cambiar 17 por User logged 
                select new Carpeta { Id = m.IdItem, Nombre = b.LOGICAL_NAME,IdParent=-1 }                
                );
            //Reports

            foreach (Carpeta cp in subjs)
            {
                Byte[] imagen_stream = new Byte[0];
                
                string pathImgs= "/Images/";
               
                BTIMAGES imgg = db.BTIMAGES.Find(cp.Id);


                if (imgg != null)
                {
                    imagen_stream = db.BTIMAGES.Where(p => p.IdItem == cp.Id).SingleOrDefault().Picture;

                    System.IO.MemoryStream ms = new System.IO.MemoryStream(imagen_stream);
                    //write to file
                    pathImgs = Request.MapPath("~/Images/") + "Img_" + cp.Id.ToString() + "_folder.jpg";
                    //pathImgs = pathImgs+ "Img_" + cp.Id.ToString() + "_folder.jpg";
                    System.IO.FileStream file = new System.IO.FileStream(pathImgs, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                    ms.WriteTo(file);
                    file.Close();
                    ms.Close();
                }
                else {
                pathImgs =pathImgs + "folder.jpg"; }
                ViewData[Convert.ToString(cp.Id)] = System.IO.Path.GetFileName(pathImgs); 

            }
            
            return subjs.AsEnumerable();
            
        }

        public ActionResult SubCarpetas(Int32 idprj,Int32 idpar)

        {
            Int32 idparFold = 0;
            BTSUBJECT_ITEMS idfolderP = db.BTSUBJECT_ITEMS.Where(a => a.IdItem == idpar).FirstOrDefault();
            if (idfolderP != null) {  idpar= idfolderP.IdSubject; }
            BTSUBJECT idfolderPar = db.BTSUBJECT.Where(a => a.IdSubject == idpar).FirstOrDefault();
            if (idfolderPar != null) { idparFold = idfolderPar.IdSubjectParent.Value; }
            

            ViewData["parFold"] = idparFold;
            ViewData["par"] = idpar;
            ViewData["prj"] = idprj;
            return View(SubCarpetas_Listar(idpar));
        }

        // GET: /Btsubjects/Details/5

        public ActionResult Details(int id = 0)
        {
            BTSUBJECT btsubject = db.BTSUBJECT.Find(id);
            if (btsubject == null)
            {
                return HttpNotFound();
            }
            return View(btsubject);
        }

        //
        // GET: /Btsubjects/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Btsubjects/Create

        [HttpPost]
        public ActionResult Create(BTSUBJECT btsubject)
        {
            if (ModelState.IsValid)
            {
                db.BTSUBJECT.Add(btsubject);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(btsubject);
        }

        //
        // GET: /Btsubjects/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BTSUBJECT btsubject = db.BTSUBJECT.Find(id);
            if (btsubject == null)
            {
                return HttpNotFound();
            }
            return View(btsubject);
        }

        //
        // POST: /Btsubjects/Edit/5

        [HttpPost]
        public ActionResult Edit(BTSUBJECT btsubject)
        {
            if (ModelState.IsValid)
            {
                db.Entry(btsubject).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(btsubject);
        }

        //
        // GET: /Btsubjects/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BTSUBJECT btsubject = db.BTSUBJECT.Find(id);
            if (btsubject == null)
            {
                return HttpNotFound();
            }
            return View(btsubject);
        }

        //
        // POST: /Btsubjects/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BTSUBJECT btsubject = db.BTSUBJECT.Find(id);
            db.BTSUBJECT.Remove(btsubject);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}