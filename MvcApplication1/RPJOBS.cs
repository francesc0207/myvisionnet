//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MvcApplication1
{
    using System;
    using System.Collections.Generic;
    
    public partial class RPJOBS
    {
        public int IDJOB { get; set; }
        public Nullable<int> IDPLAN { get; set; }
        public Nullable<int> IDPROCESS { get; set; }
        public Nullable<int> IDSERVER { get; set; }
        public Nullable<System.DateTime> INICIO { get; set; }
        public Nullable<System.DateTime> FINAL { get; set; }
        public Nullable<int> NTRY { get; set; }
        public Nullable<int> EXITCODE { get; set; }
        public Nullable<int> IDPARENTJOB { get; set; }
        public string LOGICAL_NAME { get; set; }
        public string PROCESSNAME { get; set; }
        public Nullable<int> IdProject { get; set; }
    }
}
