//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MvcApplication1
{
    using System;
    using System.Collections.Generic;
    
    public partial class OPMAILPROFILE
    {
        public int IDMAILPROFILE { get; set; }
        public string PROFILE_PASSWORD { get; set; }
        public bool NOMAIL { get; set; }
        public string PROFILE_INFO { get; set; }
        public Nullable<short> MAIL_TYPE { get; set; }
        public string NOTES_SERVER { get; set; }
        public string NOTES_DATABASE { get; set; }
        public string FORMULARIODOC { get; set; }
        public string CAMPO_TITULO { get; set; }
        public string CAMPO_MENSAJE { get; set; }
        public string CAMPO_DESTINATARIO { get; set; }
        public string CAMPO_ATTATCHMENTS { get; set; }
        public bool SEND_MAIL { get; set; }
        public bool SAVE_DOC { get; set; }
        public string Profile_UserName { get; set; }
        public string Profile_DefaultFrom { get; set; }
        public string Profile_DefaultCharset { get; set; }
        public string SmtpServer { get; set; }
        public string SmtpPort { get; set; }
        public bool SmtpRequiereAutentificacion { get; set; }
        public Nullable<int> SmtpTimeOut { get; set; }
        public Nullable<bool> SSL { get; set; }
        public Nullable<int> SmtpTipoAutentificacion { get; set; }
    }
}
