//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MvcApplication1
{
    using System;
    using System.Collections.Generic;
    
    public partial class OPPLAN
    {
        public int IDPROCESSPLAN { get; set; }
        public Nullable<int> IDPROCESS { get; set; }
        public Nullable<short> ACTSTATE { get; set; }
        public Nullable<System.DateTime> LASTEXECUTION { get; set; }
        public Nullable<System.DateTime> NEXTEXECUTION { get; set; }
        public Nullable<System.DateTime> STARTDAY { get; set; }
        public Nullable<System.DateTime> ENDDAY { get; set; }
        public Nullable<System.DateTime> STARTHOUR { get; set; }
        public Nullable<System.DateTime> ENDHOUR { get; set; }
        public Nullable<int> FREQUENCE { get; set; }
        public Nullable<int> EXECTYPE { get; set; }
        public Nullable<int> NRETRY { get; set; }
        public Nullable<int> FREQRETRY { get; set; }
        public Nullable<int> REPHOUR { get; set; }
        public Nullable<int> INTERVALHOUR { get; set; }
        public Nullable<int> REPDAY { get; set; }
        public Nullable<int> REPEAT { get; set; }
        public Nullable<int> FRECHOUR { get; set; }
        public bool ASAP { get; set; }
        public Nullable<int> IDSERVER { get; set; }
        public Nullable<int> IdProject { get; set; }
        public Nullable<int> EXECUTIONUSER { get; set; }
        public Nullable<int> EXECUTIONMODE { get; set; }
        public Nullable<short> FREQMINUTES { get; set; }
        public Nullable<short> FREQHOUR { get; set; }
        public string WEEKDAY { get; set; }
        public string MailtowhenInc { get; set; }
        public Nullable<bool> MailWhenInc { get; set; }
        public Nullable<short> StateInc { get; set; }
        public Nullable<int> PerfilMail { get; set; }
        public Nullable<System.DateTime> TimeFreqTo { get; set; }
        public Nullable<System.DateTime> TimeFreqFrom { get; set; }
        public string MailSubjectWhenInc { get; set; }
        public string MailBodywhenInc { get; set; }
    }
}
