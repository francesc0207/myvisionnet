//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MvcApplication1
{
    using System;
    using System.Collections.Generic;
    
    public partial class BTTASK
    {
        public int IDPROCESS { get; set; }
        public int IDTASK { get; set; }
        public Nullable<int> POSITION { get; set; }
        public Nullable<int> TASK_TYPE { get; set; }
        public string TASK_COMMAND { get; set; }
        public string TASK_PARAMS { get; set; }
        public bool ISENABLED { get; set; }
        public string Task_Description { get; set; }
    }
}
