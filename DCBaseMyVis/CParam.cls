
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CParam"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_ParamName As String
Private m_ParamValue As String


Public Property Let Value(ByVal newValue As String)
    m_ParamValue = newValue
End Property

Public Property Get Value() As String
    Value = m_ParamValue
End Property

Public Property Let ParamName(ByVal newValue As String)
    m_ParamName = newValue
End Property

Public Property Get ParamName() As String
    ParamName = m_ParamName
End Property



