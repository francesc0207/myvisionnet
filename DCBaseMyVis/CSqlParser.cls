'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'  Persistable = 0  'NotPersistable
'  DataBindingBehavior = 0  'vbNone
'  DataSourceBehavior  = 0  'vbNone
'  MTSTransactionMode  = 0  'NotAnMTSObject
'END
'Attribute VB_Name = "CSqlParser"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = True
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = True
'Option Explicit
Enum eBlockType
    fieldExpr
    fromExpr
    tableExpr
    whereExpr
    groupByExpr
    havingExpr
    orderByExpr
End Enum
Dim mi_step As eBlockType
Function parse(ByVal sql As String, ByRef sSelect As String, ByRef Campo As Collection, ByRef sFrom As String, ByRef Tabla As Collection, ByRef sWhere As String, _
ByRef sHaving As String, ByRef sGroupBy As String, ByRef sOrderBy As String, ByRef sError As Variant) As Boolean
    
    On Error GoTo catch
    If sql = "" Then
        sError = "Se ha intentado analizar una sentencia vacia"
        parse = False
        GoTo Finally
    End If
    'SQL = Trim(CleanBadChars(SQL))
    sql = Trim(sql)
    sql = processSelectSufix(sql, sSelect)
    
    If Not FirstTokenIs("select", sql, sql, sError) Then
        parse = False
        GoTo Finally
    End If
    
    mi_step = fieldExpr
    sql = ProcessList(sql, "from", Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy, sError)   'VBLM SKIP=1
    If Not FirstTokenIs("from", sql, sql, sError) Then    'VBLM SKIP=1
        parse = False
        GoTo Finally
    End If
    mi_step = tableExpr
    If Not parseFrom(sql, sql, Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy, sError) Then
        parse = False
        GoTo Finally
    End If
    'sql = ProcessList(sql, "where")
    If nextTokenIs("where", sql) Then
        If Not FirstTokenIs("where", sql, sql, sError) Then
            parse = False
            GoTo Finally
        End If
        mi_step = whereExpr
        If Not ProcessTextBlock(sql, Array("group by", "order by", "having"), sql, Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy, sError) Then 'VBLM SKIP=1,2,3
            parse = False
            GoTo Finally
        End If
    End If
    If nextTokenIs("group by", sql) Then    'VBLM SKIP=1
        mi_step = groupByExpr
        If Not ProcessTextBlock(sql, Array("order by", "having"), sql, Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy, sError) Then   'VBLM SKIP=1,2
            parse = False
            GoTo Finally
        End If
    End If
    If nextTokenIs("having", sql) Then    'VBLM SKIP=1
        mi_step = havingExpr
        If Not ProcessTextBlock(sql, Array("group by", "order by"), sql, Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy, sError) Then   'VBLM SKIP=1,2
            parse = False
            GoTo Finally
        End If
    End If
    If nextTokenIs("order by", sql) Then    'VBLM SKIP=1
        mi_step = orderByExpr
        If Not ProcessTextBlock(sql, Array("group by", "having"), sql, Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy, sError) Then   'VBLM SKIP=1,2
            parse = False
            GoTo Finally
        End If
    End If
    parse = True
Finally:
    Exit Function
catch:
    sError = "Error en el m�todo CSQLParser.parse. Causa: " & Err.Number & "-" & Err.Description
    parse = False
   Resume Finally
End Function
Private Function parseFrom(ByVal sSQL As String, ByRef sSqlRestante As String, ByRef Campo As Collection, ByRef sFrom As String, ByRef Tabla As Collection, _
ByRef sWhere As String, ByRef sHaving As String, ByRef sGroupBy As String, ByRef sOrderBy As String, ByRef sError As Variant) As Boolean
'VBLM OFF
    Dim tokenList As Collection
    Dim PosicioActual As Integer
    Dim ultimaPosicio As Integer
    Dim buffer As String
    Dim endToken As String
    
    On Error GoTo catch
    
    sFrom = ""
    
    Set tokenList = New Collection
    
    'PosicioActual = InStr(1, UCase(sSql), "WHERE", vbTextCompare)
    'If PosicioActual = 0 Then
    '    PosicioActual = InStr(1, UCase(sSql), "GROUP BY", vbTextCompare)
    'End If
    'If PosicioActual = 0 Then
    '    PosicioActual = InStr(1, UCase(sSql), "HAVING", vbTextCompare)
    'End If
    'If PosicioActual = 0 Then
    '    PosicioActual = InStr(1, UCase(sSql), "ORDER BY", vbTextCompare)
    'End If
    
    PosicioActual = FindToken(UCase(sSQL), "WHERE")
    If PosicioActual = 0 Then
        PosicioActual = FindToken(UCase(sSQL), "GROUP BY")
    End If
    If PosicioActual = 0 Then
        PosicioActual = FindToken(UCase(sSQL), "HAVING")
    End If
    If PosicioActual = 0 Then
        PosicioActual = FindToken(UCase(sSQL), "ORDER BY")
    End If
    
    
    If PosicioActual > 0 Then
        sFrom = Trim$(Left$(sSQL, PosicioActual - 1))
    Else
        sFrom = sSQL
    End If
    
    If FindToken(UCase(sSQL), "JOIN") = 0 Then
        PosicioActual = FindToken(UCase(sSQL), "WHERE")
        If PosicioActual <> 0 Then
            endToken = "WHERE"
        Else
            PosicioActual = FindToken(UCase(sSQL), "GROUP BY")
            If PosicioActual <> 0 Then
                endToken = "GROUP BY"
            Else
                PosicioActual = FindToken(UCase(sSQL), "HAVING")
                If PosicioActual <> 0 Then
                    endToken = "HAVING"
                Else
                    PosicioActual = FindToken(UCase(sSQL), "ORDER BY")
                    If PosicioActual <> 0 Then
                        endToken = "ORDER BY"
                    Else
                        endToken = ""
                    End If
                End If
            End If
        End If
        sSqlRestante = ProcessList(sSQL, endToken, Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy, sError)
    Else
        PosicioActual = 1
        Do
            ultimaPosicio = PosicioActual
            PosicioActual = InStr(PosicioActual + 1, sSQL, " ", vbTextCompare)
            If PosicioActual - ultimaPosicio > 0 Then
                If InStr(1, Mid$(sSQL, ultimaPosicio, PosicioActual - ultimaPosicio), "[", vbTextCompare) Then
                    PosicioActual = InStr(PosicioActual, sSQL, "]") + 1
                End If
            End If
            If PosicioActual <> 0 Then
                buffer = Trim$(Mid$(sSQL, ultimaPosicio, PosicioActual - ultimaPosicio))
                Select Case UCase(buffer)
                    Case "INNER", "JOIN", "LEFT", "RIGHT", "ON":
                        
                    Case "WHERE", "ORDER", "HAVING", "GROUP"
                        PosicioActual = 0
                    Case Else
                        If InStr(1, buffer, ".") Then
                            buffer = Left(buffer, InStr(1, buffer, ".") - 1)
                        End If
                        buffer = cleanFromToken(buffer)
                        On Error Resume Next
                        If buffer <> "" Then
                            tokenList.Add Trim$(buffer), Trim$(buffer)
                        End If
                        On Error GoTo catch
                End Select
            End If
        Loop While PosicioActual <> 0
'''        checkTables tokenList
        
        PosicioActual = FindToken(UCase(sSQL), "WHERE")
        If PosicioActual = 0 Then
            PosicioActual = FindToken(UCase(sSQL), "GROUP BY")
        End If
        If PosicioActual = 0 Then
            PosicioActual = FindToken(UCase(sSQL), "HAVING")
        End If
        If PosicioActual = 0 Then
            PosicioActual = FindToken(UCase(sSQL), "ORDER BY")
        End If
        sSqlRestante = Right$(sSQL, Len(sSQL) - PosicioActual + 1)
    End If
    parseFrom = True
'VBLM ON
    Exit Function
catch:
    sError = "Error en el m�todo CSQLParser.parseFrom. Causa: " & Err.Number & "-" & Err.Description
    parseFrom = False
    Exit Function
    Resume
    
End Function


Function ProcessTextBlock(sToParse As String, sEndToken As Variant, sSqlRestante As String, ByRef Campo As Collection, ByRef sFrom As String, ByRef Tabla As Collection, _
ByRef sWhere As String, ByRef sHaving As String, ByRef sGroupBy As String, ByRef sOrderBy As String, ByRef sError As Variant) As Boolean
Dim iStart As Long
    Dim iEnd As Long
    Dim iStr As Long
    Dim bWeAreIntoCommillas As Boolean
    Dim tempStr As String
    Dim sTokenFound As String
    
    On Error GoTo catch
    
    iStart = 1
    iEnd = Len(sToParse)
    bWeAreIntoCommillas = False
    
    For iStr = iStart To iEnd
        
        If Mid(sToParse, iStr, 1) = """" Or Mid(sToParse, iStr, 1) = "'" Then
            If bWeAreIntoCommillas Then
                bWeAreIntoCommillas = False
            Else
                bWeAreIntoCommillas = True
            End If
        End If
        If Not bWeAreIntoCommillas And nextTokenIsFrom(Mid(sToParse, iStr), sTokenFound, sEndToken) Then
            Exit For
        End If
    Next iStr
    
    If Mid(sToParse, iStart, iStr - iStart) <> "" Then
        OnIdentified Mid(sToParse, iStart, iStr - iStart), Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy
    End If
    tempStr = Mid(sToParse, iStr)
    
    sSqlRestante = tempStr
    ProcessTextBlock = True
Finally:
    Exit Function
catch:
    sError = "Error en el m�todo CServices.ProcessTextBlock. Causa: " & Err.Number & "-" & Err.Description
    ProcessTextBlock = False
    GoTo Finally
        
End Function



Private Function TrimBadChars(s As String) As String

    TrimBadChars = LTrimBadChars(RTrimBadChars(s))

End Function



Private Function LTrimBadChars(s As String) As String

    Dim car As String
    Dim i As Long
    Dim newString As String
    Dim badChars As String
    
    badChars = vbCrLf & vbTab
    
    For i = 1 To Len(s)
        car = Mid(s, i, 1)
        If "" = Trim(newString) Then
            If 0 = InStr(badChars, car) Then
                newString = newString & car
            Else
                newString = newString
            End If
        Else
            newString = newString & car
        End If
    Next
    LTrimBadChars = newString

End Function

Private Function RTrimBadChars(s As String) As String

    Dim car As String
    Dim i As Long
    Dim newString As String
    Dim badChars As String
    
    badChars = vbCrLf & vbTab
    
    For i = Len(s) To 1 Step -1
        car = Mid(s, i, 1)
        If "" = Trim(newString) Then
            If 0 = InStr(badChars, car) Then
                newString = car & newString
            Else
                newString = newString
            End If
        Else
            newString = car & newString
        End If
    Next
    RTrimBadChars = newString

End Function


Function nextTokenIsFrom(sToParse As String, sTokenFound As String, sToken As Variant) As Boolean
    Dim iToken As Long
    Dim tempStr As String
    
    For iToken = 0 To UBound(sToken)
        tempStr = sToken(iToken)
        If nextTokenIs(tempStr, sToParse) Then
            sTokenFound = sToken(iToken)
            nextTokenIsFrom = True
            Exit Function
        End If
    Next iToken
    nextTokenIsFrom = False
End Function





Function nextTokenIs(sToken As String, sToParse As String) As Boolean
    EraseLeftBadChars (sToParse)
    If Len(sToken) = 0 Then
        nextTokenIs = False
    ElseIf Len(sToParse) < Len(sToken) Then
        nextTokenIs = False
    ElseIf UCase(sToken) = UCase(Left(sToParse, Len(sToken))) Then
        nextTokenIs = True
    Else
        nextTokenIs = False
    End If
End Function

Function EraseLeftBadChars(ByVal sToParse As String)
    Dim iStr As Long
    
    If Len(sToParse) = 0 Then Exit Function
    Do
        iStr = iStr + 1
    Loop Until Asc(Mid(sToParse, iStr, 1)) > 32 Or iStr >= Len(sToParse)
        
    sToParse = Mid(sToParse, iStr)
    
End Function



Public Function cleanFromToken(sToken As String) As String
    Dim PosicioActual As Integer
    Dim ultimaPosicio As Integer
    Dim sResult As String
    sResult = sToken
    PosicioActual = 0
    Do
        PosicioActual = InStr(PosicioActual + 1, sResult, "(", vbTextCompare)
        If PosicioActual > 0 Then
            Mid(sResult, PosicioActual, 1) = " "
        End If
    Loop While PosicioActual <> 0
    Do
        PosicioActual = InStr(PosicioActual + 1, sResult, ")", vbTextCompare)
        If PosicioActual > 0 Then
            Mid(sResult, PosicioActual, 1) = " "
        End If
    Loop While PosicioActual <> 0
    cleanFromToken = sResult
End Function


Private Function FindToken(ByVal sSQL As String, ByVal token As String) As Integer

    Dim PosicioActual As Integer
    Dim carPrevio As String
    Dim carSiguiente As String
    Dim sSQLSinInnerSelect As String
    Dim diferencia As Integer
    Dim posicioParentesis As Integer

    PosicioActual = 0
    
    sSQLSinInnerSelect = RemoveInnerSelect(sSQL, posicioParentesis)
    diferencia = Len(sSQL) - Len(sSQLSinInnerSelect)
    sSQL = sSQLSinInnerSelect

    While True

        PosicioActual = InStr(PosicioActual + 1, sSQL, token, vbTextCompare)
        If PosicioActual = 0 Then
            FindToken = 0
            Exit Function
        End If
        If PosicioActual > 1 Then
            carPrevio = Mid(sSQL, PosicioActual - 1, 1)
            
        Else
            carPrevio = ""
        End If
        If Len(sSQL) > PosicioActual Then
            carSiguiente = Mid(sSQL, PosicioActual + Len(token), 1)
        Else
            carSiguiente = ""
        End If
        If (carSiguiente = "" Or IsCarSeparator(carSiguiente)) And (carPrevio = "" Or IsCarSeparator(carPrevio)) Then
            If PosicioActual > posicioParentesis Then
                FindToken = PosicioActual + diferencia
            Else
                FindToken = PosicioActual
            End If
            Exit Function
        End If
    Wend


End Function

Private Function IsCarSeparator(car As String) As Boolean

    If car = " " Then
        IsCarSeparator = True
        Exit Function
    End If

    If car = Chr(10) Or car = Chr(13) Or car = Chr(9) Then
        IsCarSeparator = True
        Exit Function
    End If

    If 0 <> InStr(",()+-*/\&=[]", car) Then
        IsCarSeparator = True
        Exit Function
    End If
    IsCarSeparator = False


End Function

Private Function RemoveInnerSelect(sSQL As String, ByRef posicioParentesi As Integer) As String
    Dim posicioPrimerParentesis As Integer
    Dim posicioUltimParentesis As Integer
    Dim n As Integer
    Dim numeroParentesisOberts As Integer
    
    posicioPrimerParentesis = InStr(1, sSQL, "(")
    If posicioPrimerParentesis > 0 Then
        'Retornarem la posicio del primer parentesis a la variable per referencia
        posicioParentesi = posicioPrimerParentesis
        'Mirem si hi ha un select
        If InStr(posicioPrimerParentesis, sSQL, "SELECT", vbTextCompare) Then
            numeroParentesisOberts = 1
            posicioUltimParentesis = -1
            For n = posicioPrimerParentesis + 1 To Len(sSQL)
                If Mid$(sSQL, n, 1) = "(" Then
                    numeroParentesisOberts = numeroParentesisOberts + 1
                ElseIf Mid$(sSQL, n, 1) = ")" Then
                    numeroParentesisOberts = numeroParentesisOberts - 1
                End If
                If numeroParentesisOberts = 0 Then
                    posicioUltimParentesis = n
                    Exit For
                End If
            Next n
            
            If posicioUltimParentesis <> -1 Then
                RemoveInnerSelect = Left$(sSQL, posicioPrimerParentesis - 1) & Right$(sSQL, Len(sSQL) - posicioUltimParentesis)
            Else
                RemoveInnerSelect = sSQL
            End If
        Else
            RemoveInnerSelect = sSQL
        End If
    Else
        RemoveInnerSelect = sSQL
    End If
    
End Function



Function ProcessList(ByVal sToParse As String, ByVal sEndToken As String, ByRef Campo As Collection, ByRef sFrom As String, ByRef Tabla As Collection, _
ByRef sWhere As String, ByRef sHaving As String, ByRef sGroupBy As String, ByRef sOrderBy As String, ByRef sError As Variant) As String
    Dim iStart As Long
    Dim iEnd As Long
    Dim iStr As Long
    Dim bWeAreIntoCommillas As Boolean
    Dim bWeAreIntoParentesis As Boolean
    Dim numeroParentesis As Integer
    Dim caracterActual As String
    
    Dim tempStr As String
    numeroParentesis = 0
    iStart = 1
    iEnd = Len(sToParse)
    bWeAreIntoCommillas = False
    
    Do
        For iStr = iStart To iEnd
            caracterActual = Mid(sToParse, iStr, 1)
            If caracterActual = "(" Or caracterActual = "[" Then
                If Not bWeAreIntoCommillas Then
                    bWeAreIntoParentesis = True
                    numeroParentesis = numeroParentesis + 1
                End If
            End If
            
            If caracterActual = ")" Or caracterActual = "]" Then
                If Not bWeAreIntoCommillas Then
                    bWeAreIntoParentesis = False
                    numeroParentesis = numeroParentesis - 1
                End If
            End If
            
                        
            
            
            If caracterActual = """" Or caracterActual = "'" Then
                If bWeAreIntoCommillas Then
                    bWeAreIntoCommillas = False
                Else
                    bWeAreIntoCommillas = True
                End If
            End If
            If Not bWeAreIntoCommillas And numeroParentesis = 0 And caracterActual = "," Then
                OnIdentified Mid(sToParse, iStart, iStr - iStart), Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy
                Exit For
            End If
            If Not bWeAreIntoCommillas And numeroParentesis = 0 And nextTokenIs(sEndToken, Mid(sToParse, iStr)) Then
                If iStr > 1 And IsCarSeparator(Mid(sToParse, iStr - 1, 1)) Then
                    If Len(sEndToken) > 0 Then
                        OnIdentified Mid(sToParse, iStart, iStr - iStart), Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy
                        tempStr = Mid(sToParse, iStr)
                        ProcessList = tempStr
                    Else
                        OnIdentified sToParse, Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy
                        tempStr = ""
                        ProcessList = ""
                    End If
                    Exit Function
                End If
                
            End If
        Next iStr
            
        If Not iStr >= iEnd Then
            iStart = iStr + 1  'Ens saltem la coma
        End If
    Loop Until iStr >= iEnd
    
    
    OnIdentified Mid(sToParse, iStart, iStr - iStart), Campo, sFrom, Tabla, sWhere, sHaving, sGroupBy, sOrderBy
    ProcessList = ""
    
End Function



Function FirstTokenIs(ByVal sToken As String, ByVal sToParse As String, ByRef sSqlRestante As String, ByRef sError As Variant) As Boolean

    Dim cleanToken As String
    Dim cleanToParse As String
    Dim posToken As Long


    cleanToParse = CleanBadChars(sToParse)

    If Len(cleanToParse) < Len(sToken) Then
        sError = "Se esperaba un elemento '" & sToken & "' al inicio de la cadena " & sToParse
        FirstTokenIs = False
        Exit Function
    End If
    
    
    If UCase(sToken) = UCase(Left(cleanToParse, Len(sToken))) Then
        posToken = InStr(UCase(sToParse), UCase(sToken))
        sSqlRestante = Mid(sToParse, posToken + Len(sToken) + 1)
        FirstTokenIs = True
    Else
        sError = "Se esperaba un elemento '" & sToken & "' al inicio de la cadena " & sToParse
        FirstTokenIs = False
    End If
End Function

Public Function CleanBadChars(ByVal s As String) As String

    Dim car As String
    Dim i As Long
    Dim newString As String
    Dim badChars As String
    
    badChars = vbCrLf & vbTab
    
    For i = 1 To Len(s)
        car = Mid(s, i, 1)
        If 0 = InStr(badChars, car) Then
            newString = newString & car
        Else
            newString = newString & " "
        End If
    Next
    CleanBadChars = newString

End Function

Function processSelectSufix(ByVal sSQL As String, ByRef sSelect As String) As String
    Dim buffer As String
'VBLM OFF
    Dim Posicio As Integer
    
    Posicio = InStr(1, sSQL, "SELECT UNIQUE")
    If Posicio > 0 Then
        sSelect = "UNIQUE"
''        mi_interesado.identificadoSelect ("UNIQUE")
        processSelectSufix = "SELECT " & Right(sSQL, Len(sSQL) - Len("SELECT UNIQUE"))
    Else
        Posicio = InStr(1, sSQL, "SELECT DISTINCTROW")
        If Posicio > 0 Then
            sSelect = "DISTINCTROW"
'''            mi_interesado.identificadoSelect ("DISTINCTROW")
            processSelectSufix = "SELECT " & Right(sSQL, Len(sSQL) - Len("SELECT DISTINCTROW"))
        Else
            Posicio = InStr(1, sSQL, "SELECT DISTINCT")
            If Posicio > 0 Then
                sSelect = "DISTINCT"
'''                mi_interesado.identificadoSelect ("DISTINCT")
                processSelectSufix = "SELECT " & Right(sSQL, Len(sSQL) - Len("SELECT DISTINCT"))
            Else
                'Dim tokenList As New Collection
                'Dim topToken As String
                'If Me.GetTokenList(sSQL, tokenList) Then
                '    If tokenList.Count >= 3 Then
                '        topToken = tokenList.item(3)
                '    End If
                'End If
                buffer = GetTopExpression(sSQL)
                'buffer = BuscarCadena(sSQL, "SELECT TOP * ", 1)
                buffer = Trim(buffer)
                If Len(buffer) > 0 Then
                    sSelect = "TOP " & buffer & " "
'''                    mi_interesado.identificadoSelect ("TOP " & buffer & " ")
                    processSelectSufix = "SELECT " & Right(sSQL, Len(sSQL) - Len("SELECT TOP " & buffer & " "))
                Else
                    processSelectSufix = sSQL
                End If
            End If
        End If
    End If
'VBLM ON
End Function
Sub OnIdentified(ByVal sExpr As String, ByRef Campo As Collection, ByRef sFrom As String, ByRef Tabla As Collection, ByRef sWhere As String, ByRef sHaving As String, _
ByRef sGroupBy As String, ByRef sOrderBy As String)
    If sExpr <> "" Then
        'aExpr = purgeBadChars(aExpr)
        'aExpr = Trim(CleanBadChars(aExpr))
        Select Case mi_step
            Case fieldExpr
                'aExpr = Trim(CleanBadChars(aExpr))
                sExpr = Trim(TrimBadChars(sExpr))
                Campo.Add sExpr
''                mi_interesado.identificadoCampo aExpr
            Case tableExpr
                sExpr = Trim(CleanBadChars(sExpr))
                'mi_interesado.identificadaTabla processField(aExpr)
                Tabla.Add sExpr
'''                mi_interesado.identificadaTabla aExpr
            Case whereExpr
                sWhere = sExpr
''                mi_interesado.identificadoWhere aExpr
            Case groupByExpr
                If UCase(Left(sExpr, 8)) = "GROUP BY" Then
                    sGroupBy = Trim(Right(sExpr, Len(sExpr) - 8))
                Else
                    sGroupBy = sExpr
                End If
'''                mi_interesado.identificadoGroupBy aExpr
            Case havingExpr
                If UCase(Left(sExpr, 6)) = "HAVING" Then
                    sHaving = Trim(Right(sExpr, Len(sExpr) - 6))
                Else
                    sHaving = sExpr
                End If
'''                mi_interesado.identificadoHaving aExpr
            Case orderByExpr
                If UCase(Left(sExpr, 8)) = "ORDER BY" Then
                    sOrderBy = Trim(Right(sExpr, Len(sExpr) - 8))
                Else
                    sOrderBy = sExpr
                End If
''                mi_interesado.identificadoOrderBy aExpr
            Case fromExpr
                sFrom = Trim(CleanBadChars(sExpr))
''                mi_interesado.identificadoFrom aExpr
        End Select
    End If
End Sub




Function GetTopExpression(sSQL As String) As String

    Dim posTop  As Long
    Dim topExpr As String
    
    posTop = InStr(1, UCase(sSQL), " TOP ")    'VBLM SKIP=1
    If posTop = 0 Then
        GetTopExpression = ""
        Exit Function
    End If
    If GetNextExpression(sSQL, posTop + 4, topExpr) Then
        GetTopExpression = topExpr
    Else
        GetTopExpression = ""
    End If
    
End Function


Function GetNextExpression(sSQL As String, startPos As Long, nextExpr) As Boolean

    Dim nextSql As String
    Dim tokenList As New Collection
    
    nextSql = Right(sSQL, Len(sSQL) - startPos)
    If Not Me.GetTokenList(nextSql, tokenList, False) Then
        GetNextExpression = False
        Exit Function
    End If
    
    Dim iToken As Long
    Dim lastToken As String
    Dim token As String
    Dim expr As String
    
    Dim bOpenOperator As Boolean
    Dim bBlankSpace As Boolean
    Dim numOpenParentesis As Long
    Dim numOpenComillaDoble As Long
    Dim numOpenComillaSimple As Long
        
    bOpenOperator = False
    bBlankSpace = False
    
    For iToken = 1 To tokenList.Count
        token = tokenList.Item(iToken)
                                
        'If iToken = 1 Then
        '    lastToken = token
        '    expr = token
        '    GoTo NextIter
        'End If
        If numOpenComillaDoble > 0 Then
            expr = expr & token
            If token = """" Then
                numOpenComillaDoble = numOpenComillaDoble - 1
            End If
            GoTo NextIter
        End If
        If numOpenComillaSimple > 0 Then
            expr = expr & token
            If token = "'" Then
                numOpenComillaSimple = numOpenComillaSimple - 1
            End If
            GoTo NextIter
        End If
        If numOpenParentesis > 0 Then
            expr = expr & token
            If token = ")" Then
                numOpenParentesis = numOpenParentesis = -1
                bOpenOperator = False
            End If
            GoTo NextIter
        End If
                
        If IsTokenBlank(token) Then
            expr = expr & token
            GoTo NextIter
        End If
        
        If IsTokenCRLF(token) Then
            expr = expr & token
            GoTo NextIter
        End If
                
        If token = """" Then
            If numOpenComillaDoble > 0 Then
                numOpenComillaDoble = numOpenComillaDoble - 1
            Else
                If Not bOpenOperator Then
                    Exit For
                End If
                numOpenComillaDoble = numOpenComillaDoble + 1
            End If
            expr = expr & token
            GoTo NextIter
        End If
        If token = "'" Then
            If numOpenComillaSimple > 0 Then
                numOpenComillaSimple = numOpenComillaSimple - 1
            Else
                If Not bOpenOperator Then
                    Exit For
                End If
                numOpenComillaSimple = numOpenComillaSimple + 1
            End If
            expr = expr & token
            GoTo NextIter
        End If
        
        If token = "(" Then
            numOpenParentesis = numOpenParentesis + 1
            If lastToken = "@PAR" Then    'VBLM SKIP=1
                expr = expr & token
                bOpenOperator = True
                GoTo NextIter
            End If
            If expr = "" Then
                expr = expr & token
                bOpenOperator = True
                GoTo NextIter
            Else
                If Not bOpenOperator Then
                    Exit For
                End If
            End If
        End If
        
        If bOpenOperator Then
            expr = expr & token
            bOpenOperator = False
            GoTo NextIter
        End If
                        
        If IsTokenOperatorConcatenator(token) Then
            expr = expr & token
            bOpenOperator = True
            GoTo NextIter
        End If
        
        If bOpenOperator Then
            expr = expr & token
            bOpenOperator = False
            GoTo NextIter
        Else
            If expr = "" Then
                expr = token
            Else
                Exit For
            End If
        End If
NextIter:
        lastToken = token
        If IsTokenBlank(token) Then
            bBlankSpace = True
        Else
            bBlankSpace = False
        End If
    Next
    nextExpr = expr
    GetNextExpression = True

End Function
Function IsTokenOperatorConcatenator(ByVal token As String) As Boolean
'VBLM OFF

    If 0 <> InStr("+-*/\&=[", token) Then
        IsTokenOperatorConcatenator = True
        Exit Function
    End If
    Select Case UCase(token)
        Case "AND", "OR", "NOT", "XOR"
            IsTokenOperatorConcatenator = True
            Exit Function
    End Select
    IsTokenOperatorConcatenator = False
    Exit Function

'VBLM ON
End Function



Function IsTokenCRLF(ByVal token As String) As Boolean
    
    Dim car As String
    Dim iCar As Long
    Dim maxLen As Long
    
    maxLen = Len(token)
    For iCar = 1 To maxLen
        car = Mid(token, iCar, 1)
        If Asc(car) = 13 Or Asc(car) = 10 Then
            IsTokenCRLF = True
            Exit Function
        End If
    Next
    IsTokenCRLF = False
    Exit Function

End Function


Function IsTokenBlank(ByVal token As String) As Boolean
    
    Dim car As String
    Dim iCar As Long
    Dim maxLen As Long
    
    maxLen = Len(token)
    For iCar = 1 To maxLen
        car = Mid(token, iCar, 1)
        If car <> " " Then
            IsTokenBlank = False
            Exit Function
        End If
    Next
    IsTokenBlank = True
    Exit Function

End Function



Function GetTokenList(sqlToParse As String, tokenList As Collection, Optional excludeBlanks As Boolean = True) As Boolean

    On Error GoTo catch

    Dim iStart As Long
    Dim iEnd As Long
    Dim iStr As Long
    Dim bWeAreIntoParentesis As Boolean
    Dim bStartNewToken As Boolean
    Dim numeroParentesis As Integer
    Dim caracterActual As String
    Dim posnextcomilla  As Long
    Dim posLastBlank As Long
    Dim posNextSeparator As Long
    
    Dim currentToken As String
    Dim posStartCurrentToken As Long
    
    Dim tempStr As String
    numeroParentesis = 0
    iStart = 1
    iEnd = Len(sqlToParse)
    
    bStartNewToken = True
    
    posStartCurrentToken = 1
    
    For iStr = iStart To iEnd
        caracterActual = Mid(sqlToParse, iStr, 1)
        
        If IsCarSeparator(caracterActual) Then
            If caracterActual = " " Then
                posLastBlank = GetLastCarOcurrence(sqlToParse, iStr + 1, " ")
                If posLastBlank <> 0 Then
                    currentToken = Mid(sqlToParse, iStr, posLastBlank - iStr)
                    iStr = posLastBlank
                Else
                    currentToken = caracterActual
                End If
                If Not excludeBlanks Then
                    tokenList.Add currentToken      'deliberdament no afegim els espais en blanc
                End If
                posStartCurrentToken = iStr + 1
            Else
                currentToken = caracterActual
                tokenList.Add currentToken
                'iStr = posnextcomilla
                posStartCurrentToken = iStr + 1
            End If
        Else
            posNextSeparator = GetNextSeparator(sqlToParse, iStr)
            If posNextSeparator <> iStr Then
                currentToken = Mid(sqlToParse, iStr, posNextSeparator - iStr)
                tokenList.Add currentToken
                iStr = posNextSeparator - 1
                posStartCurrentToken = iStr + 1
            Else
                currentToken = caracterActual
                tokenList.Add currentToken
            End If
        End If
    Next iStr
    GetTokenList = True
    Exit Function
catch:
    GetTokenList = False
    Exit Function
        
    
End Function
Function GetNextSeparator(sqlToParse As String, posInit As Long) As Long
 
 Dim maxLen As Long
 Dim iCar As Long
 Dim currentCar As String
 
 maxLen = Len(sqlToParse)
 
 For iCar = posInit To maxLen
     currentCar = Mid(sqlToParse, iCar, 1)
     If IsCarSeparator(currentCar) Then
        GetNextSeparator = iCar
        Exit Function
     End If
 Next
 GetNextSeparator = maxLen + 1
 
End Function

Private Function GetLastCarOcurrence(cadena, posInicial As Long, car As String) As Long

    Dim maxLen As Long
    Dim currentCar As String
    Dim iCar As Long
    Dim LastOcurrence As Long
    
    maxLen = Len(cadena)
    GetLastCarOcurrence = 0
    
    For iCar = posInicial To maxLen
        currentCar = Mid(cadena, iCar, 1)
        If currentCar = car Then
            GetLastCarOcurrence = iCar
        Else
            Exit Function
        End If
    Next
    'GetLastCarOcurrence = LastOcurrence
    Exit Function

End Function





