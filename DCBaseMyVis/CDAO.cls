VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDAO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const MSACCESS = "Microsoft Access"
Private Const DBASEIII = "Dbase III;"
Private Const DBASEIV = "Dbase IV;"
Private Const DBASE5 = "Dbase 5.0;"
Private Const FOXPRO20 = "FoxPro 2.0;"
Private Const FOXPRO25 = "FoxPro 2.5;"
Private Const FOXPRO26 = "FoxPro 2.6;"
Private Const FOXPRO30 = "FoxPro 3.0;"
Private Const PARADOX3X = "Paradox 3.X;"
Private Const PARADOX4X = "Paradox 4.X;"
Private Const PARADOX5X = "Paradox 5.X;"
Private Const BTRIEVE = "Btrieve;"
Private Const EXCEL30 = "Excel 3.0;"
Private Const EXCEL40 = "Excel 4.0;"
Private Const EXCEL50 = "Excel 5.0;"
Private Const TEXTFILES = "Text;"
Private Const SQLDB = "ODBC;"
Private Const LOTUSWK4 = "Lotus WK4;"

Private Const DB_TYPE_ACCESS = 1
Private Const DB_TYPE_TEXT = 2
Private Const DB_TYPE_EXCEL = 3
Private Const DB_TYPE_ODBC = 4
Private Const DB_TYPE_DBASE_III = 5
Private Const DB_TYPE_DBASE_IV = 6
Private Const DB_TYPE_DBASE_V = 7
Private Const DB_TYPE_WK4 = 8

Private gTempEngine As DBEngine
Private gTempWorkspace As Workspace

Private Function ConvertDAORecordsetToXML(ByRef aDAORS As DAO.recordset, ByRef sError As Variant) As String
    Dim oXMLRecordset As ADODB.recordset
    Dim oDAOField As DAO.Field
    Dim oStream As ADODB.Stream
    
    On Error GoTo catch
    
    Set oXMLRecordset = New ADODB.recordset
    
    For Each oDAOField In aDAORS.Fields
        If oDAOField.Type <> dbMemo Then
             oXMLRecordset.Fields.Append oDAOField.Name, DAOTypeToADOType(oDAOField.Type), oDAOField.Size, adFldIsNullable
        End If
    Next oDAOField
    
    oXMLRecordset.CursorLocation = adUseClient
    oXMLRecordset.Open
    
    While Not aDAORS.EOF
        oXMLRecordset.AddNew
        For Each oDAOField In aDAORS.Fields
            If oDAOField.Type <> dbMemo Then
                oXMLRecordset.Fields(oDAOField.Name) = oDAOField.Value
            End If
        Next oDAOField
        oXMLRecordset.Update
        aDAORS.MoveNext
    Wend
    
    oXMLRecordset.MoveFirst
    
    
    Set oStream = New ADODB.Stream
    oXMLRecordset.Save oStream, adPersistXML
    ConvertDAORecordsetToXML = oStream.ReadText
    
Finally:
    
    If Not oStream Is Nothing Then
        If oStream.State <> adStateClosed Then
            oStream.Close
        End If
        Set oStream = Nothing
    End If

    If Not oXMLRecordset Is Nothing Then
        If oXMLRecordset.State <> adStateClosed Then
            oXMLRecordset.Close
        End If
        Set oXMLRecordset = Nothing
    End If
    
    Exit Function
catch:
    sError = "Error en el m�todo CDAO.ConvertDAORecordsetToXML. Causa: " & Err.Number & "-" & Err.Description
    Resume Finally
    Resume
End Function

Private Function DAOTypeToADOType(ByRef DAOType As DAO.DataTypeEnum) As ADODB.DataTypeEnum
    Select Case DAOType
        Case dbBigInt: DAOTypeToADOType = adBigInt
        Case dbBinary: DAOTypeToADOType = adBinary
        Case dbBoolean: DAOTypeToADOType = adBoolean
        Case dbByte: DAOTypeToADOType = adSmallInt
        Case dbCurrency: DAOTypeToADOType = adCurrency
        Case dbChar: DAOTypeToADOType = adChar
        Case dbDate:  DAOTypeToADOType = adDBDate
        Case dbDecimal: DAOTypeToADOType = adDecimal
        Case dbDouble: DAOTypeToADOType = adDouble
        Case dbFloat: DAOTypeToADOType = adNumeric
        Case dbGUID: DAOTypeToADOType = adGUID
        Case dbInteger: DAOTypeToADOType = adInteger
        Case dbLong: DAOTypeToADOType = adInteger
        Case dbLongBinary: DAOTypeToADOType = adLongVarBinary
        Case dbMemo: DAOTypeToADOType = adVarChar
        Case dbNumeric: DAOTypeToADOType = adNumeric
        Case dbSingle: DAOTypeToADOType = adSingle
        Case dbText: DAOTypeToADOType = adVarChar
        Case dbTime: DAOTypeToADOType = adDBTime
        Case dbTimeStamp: DAOTypeToADOType = adDBTimeStamp
        Case dbVarBinary: DAOTypeToADOType = adLongVarBinary
        Case Else: DAOTypeToADOType = adIUnknown
    End Select
    
    
End Function

Public Function ExecuteInODBCDatabase(ByVal sSQL As String, ByVal sSQLPRE As String, ByVal sSQLPOST As String, ByVal sConnectionString As String, ByVal sCursorType As String, ByRef sError As Variant) As String
    Dim oDB As DAO.Database
    Dim oRS As DAO.recordset
    
    On Error GoTo catch1
    
    Set oDB = DBEngine.OpenDatabase("", dbDriverNoPrompt, False, sConnectionString)
    
    On Error GoTo catch4
    
    If Len(sSQLPRE) > 0 Then
        oDB.Execute sSQLPRE, dbSQLPassThrough
    End If

    On Error GoTo catch2
    
    Set oRS = oDB.OpenRecordset(sSQL, sCursorType, dbSQLPassThrough)
    
    On Error GoTo catch3
    
    ExecuteInODBCDatabase = ConvertDAORecordsetToXML(oRS, sError)
    
    On Error GoTo catch5
    
    If Len(sSQLPOST) > 0 Then
        oDB.Execute sSQLPOST, dbSQLPassThrough
    End If
    
finally2:
    If Not oRS Is Nothing Then
        oRS.Close
        Set oRS = Nothing
    End If
    
finally1:
    If Not oDB Is Nothing Then
        oDB.Close
        Set oDB = Nothing
    End If
    
Finally:
    Exit Function
    
catch1:
    sError = "Error en el m�todo CDAO.ExecuteInODBCDatabase1. Causa: " & Err.Number & "-" & Err.Description
    Resume Finally
catch2:
    sError = "Error en el m�todo CDAO.ExecuteInODBCDatabase2. Causa: " & Err.Number & "-" & Err.Description
    Resume finally1
catch3:
    sError = "Error en el m�todo CDAO.ExecuteInODBCDatabase3. Causa: " & Err.Number & "-" & Err.Description
    Resume finally2
catch4:
    sError = "Error en el m�todo CDAO.ExecuteInODBCDatabase4(SQL PRE). Causa: " & Err.Number & "-" & Err.Description
    Resume finally1
catch5:
    sError = "Error en el m�todo CDAO.ExecuteInODBCDatabase5(SQL POST). Causa: " & Err.Number & "-" & Err.Description
    Resume finally2
    
End Function


Public Function ExecuteTextDatabase(ByVal sSQL As String, ByVal sSQLPRE As String, ByVal sSQLPOST As String, ByVal sFileName As String, ByVal sCursorType As String, sError As Variant) As String
    Dim oDB As DAO.Database
    Dim oRS As DAO.recordset
    
    
    On Error GoTo catch4
    
    If Len(sSQLPRE) > 0 Then
        oDB.Execute sSQLPRE, dbSQLPassThrough
    End If
     
    On Error GoTo catch1
    
    Set oDB = DBEngine.OpenDatabase(sFileName, False, False, TEXTFILES)
    
    On Error GoTo catch2
    
    Set oRS = oDB.OpenRecordset(sSQL, sCursorType)
    
    On Error GoTo catch3
    
    ExecuteTextDatabase = ConvertDAORecordsetToXML(oRS, sError)
    
    
    On Error GoTo catch5
    
    If Len(sSQLPRE) > 0 Then
        oDB.Execute sSQLPRE, dbSQLPassThrough
    End If
    
    
    
finally2:
    If Not oRS Is Nothing Then
        oRS.Close
        Set oRS = Nothing
    End If
finally1:
    If Not oDB Is Nothing Then
        oDB.Close
        Set oDB = Nothing
    End If
Finally:
    Exit Function
catch1:
    sError = "Error en el m�todo CDAO.ExecuteTextDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume Finally
catch2:
    sError = "Error en el m�todo CDAO.ExecuteTextDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally1
catch3:
    sError = "Error en el m�todo CDAO.ExecuteTextDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally2
catch4:
    sError = "Error en el m�todo CDAO.ExecuteTextDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally1
catch5:
    sError = "Error en el m�todo CDAO.ExecuteTextDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally2


End Function


Public Function ExecuteInExcelDatabase(ByVal sSQL As String, ByVal sSQLPRE As String, ByVal sSQLPOST As String, ByVal sFileName As String, ByVal sPassword As String, ByVal sConnectionStringOptions As String, ByVal sCursorType As String, ByRef sError As Variant) As String
    Dim oDB As DAO.Database
    Dim oRS As DAO.recordset
    Dim sStringPassword As String
    
    On Error GoTo catch1
    
    If sPassword <> "" Then
        sStringPassword = "PWD=" & sPassword
    End If
    If sConnectionStringOptions = "" Then
        sConnectionStringOptions = "Excel 8.0;HDR=YES;IMEX=2"
    End If
    If sStringPassword <> "" Then
        If 0 = InStr(1, sConnectionStringOptions, sStringPassword) Then
            sConnectionStringOptions = sConnectionStringOptions & ";" & sStringPassword
        End If
    End If
    
    On Error GoTo catch2
    Set oDB = DBEngine.OpenDatabase(sFileName, False, False, sConnectionStringOptions & ";DATABASE=" & sFileName)
            
              
    On Error GoTo catch4
   
    If Len(sSQLPRE) > 0 Then
        oDB.Execute sSQLPRE ', dbSQLPassThrough
    End If
               
           
    On Error GoTo catch3
    Set oRS = oDB.OpenRecordset(sSQL, sCursorType)
    ExecuteInExcelDatabase = ConvertDAORecordsetToXML(oRS, sError)
    
     On Error GoTo catch5
   
    If Len(sSQLPOST) > 0 Then
        oDB.Execute sSQLPOST ', dbSQLPassThrough
    End If
      
    
finally2:
    If Not oRS Is Nothing Then
        oRS.Close
        Set oRS = Nothing
    End If
    
finally1:
    If Not oDB Is Nothing Then
        oDB.Close
        Set oDB = Nothing
    End If
    
Finally:
    Exit Function
    
catch1:
    sError = "Error en el m�todo CDAO.ExecuteInExcelDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume Finally
catch2:
    sError = "Error en el m�todo CDAO.ExecuteInExcelDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally1
catch3:
    sError = "Error en el m�todo CDAO.ExecuteInExcelDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally2
catch4:
    sError = "Error en el m�todo CDAO.ExecuteInExcelDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally2
catch5:
    sError = "Error en el m�todo CDAO.ExecuteInExcelDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally2
End Function

Public Function executeInDBaseIIIDatabase(ByVal sSQL As String, ByVal sFileName As String, ByVal sCursorType As String, ByRef sError As Variant) As String
    Dim oDB As DAO.Database
    Dim oRS As DAO.recordset
    
    On Error GoTo catch1
    Set oDB = DBEngine.OpenDatabase(sFileName, False, False, DBASEIII)
    
    On Error GoTo catch2
    Set oRS = oDB.OpenRecordset(sSQL, sCursorType)
    
    On Error GoTo catch3
    executeInDBaseIIIDatabase = ConvertDAORecordsetToXML(oRS, sError)
    
finally2:
    If Not oRS Is Nothing Then
        oRS.Close
        Set oRS = Nothing
    End If
    
finally1:
    If Not oDB Is Nothing Then
        oDB.Close
        Set oDB = Nothing
    End If
    
Finally:
    Exit Function

catch1:
    sError = "Error en el m�todo CDAO.executeInDBaseIIIDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume Finally
catch2:
    sError = "Error en el m�todo CDAO.executeInDBaseIIIDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally1
catch3:
    sError = "Error en el m�todo CDAO.executeInDBaseIIIDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally2
End Function

Public Function ExecuteInDBaseIVDatabase(ByVal sSQL As String, ByVal sFileName As String, ByVal sCursorType As String, ByRef sError As Variant) As String
    Dim oDB As DAO.Database
    Dim oRS As DAO.recordset
    
    On Error GoTo catch1
    Set oDB = DBEngine.OpenDatabase(sFileName, False, False, DBASEIV)
    
    On Error GoTo catch2
    Set oRS = oDB.OpenRecordset(sSQL, sCursorType)
    
    On Error GoTo catch3
    ExecuteInDBaseIVDatabase = ConvertDAORecordsetToXML(oRS, sError)
    
finally2:
    If Not oRS Is Nothing Then
        oRS.Close
        Set oRS = Nothing
    End If
    
finally1:
    If Not oDB Is Nothing Then
        oDB.Close
        Set oDB = Nothing
    End If

Finally:
    Exit Function
    
catch1:
    sError = "Error en el m�todo CDAO.ExecuteInDBaseIVDatabase . Causa: " & Err.Number & "-" & Err.Description
    Resume Finally
catch2:
    sError = "Error en el m�todo CDAO.ExecuteInDBaseIVDatabase . Causa: " & Err.Number & "-" & Err.Description
    Resume finally1
catch3:
    sError = "Error en el m�todo CDAO.ExecuteInDBaseIVDatabase . Causa: " & Err.Number & "-" & Err.Description
    Resume finally2
End Function

Public Function ExecuteInDBaseVDatabase(ByVal sSQL As String, ByVal sFileName As String, ByVal sCursorType As String, ByRef sError As Variant) As String
    Dim oDB As DAO.Database
    Dim oRS As DAO.recordset
    
    On Error GoTo catch1
    Set oDB = DBEngine.OpenDatabase(sFileName, False, False, DBASE5)
    
    On Error GoTo catch2
    Set oRS = oDB.OpenRecordset(sSQL, sCursorType)
    
    On Error GoTo catch3
    ExecuteInDBaseVDatabase = ConvertDAORecordsetToXML(oRS, sError)
    
finally2:
    If Not oRS Is Nothing Then
        oRS.Close
        Set oRS = Nothing
    End If
    
finally1:
    If Not oDB Is Nothing Then
        oDB.Close
        Set oDB = Nothing
    End If

Finally:
    Exit Function
    
catch1:
    sError = "Error en el m�todo CDAO.ExecuteInDBaseVDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume Finally
catch2:
    sError = "Error en el m�todo CDAO.ExecuteInDBaseVDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally1
catch3:
    sError = "Error en el m�todo CDAO.ExecuteInDBaseVDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally2
End Function

Public Function ExecuteInWK4Database(ByVal sSQL As String, ByVal sFileName As String, ByVal sCursorType As String, ByRef sError As Variant) As String
    Dim oDB As DAO.Database
    Dim oRS As DAO.recordset
    
    On Error GoTo catch1
    Set oDB = DBEngine.OpenDatabase(sFileName, False, False, LOTUSWK4)
    
    On Error GoTo catch2
    Set oRS = oDB.OpenRecordset(sSQL, sCursorType)
    
    On Error GoTo catch3
    ExecuteInWK4Database = ConvertDAORecordsetToXML(oRS, sError)
    
finally2:
    If Not oRS Is Nothing Then
        oRS.Close
        Set oRS = Nothing
    End If
    
finally1:
    If Not oDB Is Nothing Then
        oDB.Close
        Set oDB = Nothing
    End If

Finally:
    Exit Function
    
catch1:
    sError = "Error en el m�todo CDAO.ExecuteInWK4Database. Causa: " & Err.Number & "-" & Err.Description
    Resume Finally
catch2:
    sError = "Error en el m�todo CDAO.ExecuteInWK4Database. Causa: " & Err.Number & "-" & Err.Description
    Resume finally1
catch3:
    sError = "Error en el m�todo CDAO.ExecuteInWK4Database. Causa: " & Err.Number & "-" & Err.Description
    Resume finally2
End Function

Public Function ExecuteInAccessDatabase(ByVal sSQL As String, ByVal sSQLPRE As String, ByVal sSQLPOST As String, ByVal sFileName As String, ByVal sSystemFile As String, ByVal sUser As String, ByVal sPassword As String, ByVal sCursorType As String, ByRef sError As Variant) As String
    Dim oDB As DAO.Database
    Dim oRS As DAO.recordset
    Dim oWorkspace As DAO.Workspace
    
    On Error GoTo catch1
    
    If Len(sSystemFile) > 0 Then
        DBEngine.SystemDB = sSystemFile
        If Len(sUser) > 0 Then
            DBEngine.DefaultUser = sUser
        End If
        
        If Len(sPassword) > 0 Then
            DBEngine.DefaultPassword = sPassword
        End If
        
        Set oWorkspace = DBEngine.CreateWorkspace("Temp", sUser, sPassword)
        DBEngine.Workspaces.Append oWorkspace
        Set oDB = oWorkspace.OpenDatabase(sFileName, False, False)
    Else
        Set oDB = DBEngine.OpenDatabase(sFileName, False, False)
    End If
  
    On Error GoTo catch4
   
    If Len(sSQLPRE) > 0 Then
        oDB.Execute sSQLPRE ', dbSQLPassThrough
    End If
     
    On Error GoTo catch2
    
    Set oRS = oDB.OpenRecordset(sSQL, sCursorType)
    
    On Error GoTo catch3
    
    ExecuteInAccessDatabase = ConvertDAORecordsetToXML(oRS, sError)
    
    On Error GoTo catch5
   
    If Len(sSQLPOST) > 0 Then
        oDB.Execute sSQLPOST ', dbSQLPassThrough
    End If
    
    
finally2:
    If Not oRS Is Nothing Then
        oRS.Close
        Set oRS = Nothing
    End If
   
finally1:
    If Not oDB Is Nothing Then
        oDB.Close
        Set oDB = Nothing
    End If

Finally:
    Exit Function
    
catch1:
    sError = "Error en el m�todo CDAO.ExecuteInAccessDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume Finally
catch2:
    sError = "Error en el m�todo CDAO.ExecuteInAccessDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally1
catch3:
    sError = "Error en el m�todo CDAO.ExecuteInAccessDatabase. Causa: " & Err.Number & "-" & Err.Description
    Resume finally2
catch4:
    sError = "Error en el m�todo CDAO.ExecuteInAccessDatabase(SQL PRE). Causa: " & Err.Number & "-" & Err.Description
    Resume finally1
catch5:
    sError = "Error en el m�todo CDAO.ExecuteInAccessDatabase(SQL POST). Causa: " & Err.Number & "-" & Err.Description
    Resume finally2
End Function


