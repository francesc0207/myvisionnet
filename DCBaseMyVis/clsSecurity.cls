VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSecurity"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'array of alphabetic characters for encryption

Private Const START_ASCII = 32
Private Const LAST_ASCII = 126

Private Matrix(LAST_ASCII - START_ASCII, LAST_ASCII - START_ASCII) As Byte
'###########################################
'### Public Properties, Subs & Functions ###
'###########################################
Public Function Encode(pMessage As String, pKeyWord As String) As String
  


Encode = Encrypt(pMessage, pKeyWord)

End Function

Public Function Decode(pEncoded As String, pKeyWord As String) As String
   
    'return the decoded value of EnCoded Message
    Decode = Decrypt(pEncoded, pKeyWord)
    
End Function

'###########################################
'### Private Properties, Subs & Functions ###
'###########################################
Private Sub BuildMatrix()
    Dim lColumn  As Integer
    Dim lRow     As Integer
    Dim lCharVal As Integer

'first character in the matrix is 'A'
lCharVal = START_ASCII
'cycle through all columns
For lColumn = 0 To UBound(Matrix)
    'for each column, cycle through all rows
    For lRow = 0 To UBound(Matrix)
        'insert the character's ascii value at the intersection
        Matrix(lColumn, lRow) = lCharVal
        'move on to the next character
        lCharVal = lCharVal + 1
        'if character > Z then cycle back to A
        If lCharVal = LAST_ASCII + 1 Then lCharVal = START_ASCII
    Next lRow
    'the next column must start with 1 character higher than the current
    'column so increment the character
    lCharVal = lCharVal + 1
Next lColumn

End Sub

Private Function CaseDecode(pDecoded As String, _
 CaseCode As String) As String
'Code will look like this
'First Element is the number of Words
'Other elements indictate case of each letter
'1 = UPPERCASE
'0 = lowercase
'eg:
'     FailSafe Systems
'   2,10001000,1000000
'
Dim iWords    As Integer
Dim sWord     As String
Dim sCode     As String
Dim arrCode() As String
Dim x         As Integer
Dim n         As Integer

CaseDecode = ""
sCode = CaseCode
sCode = Replace(sCode, " ", "")

'If the lengths do not match then we cannot continue
If Len(pDecoded) <> Len(sCode) Then Exit Function

'First get everything in the proper case
For x = 1 To Len(sCode)
    If Mid(sCode, x, 1) = "1" Then
        Mid(pDecoded, x, 1) = UCase(Mid(pDecoded, x, 1))
    ElseIf Mid(sCode, x, 1) = "0" Then
        Mid(pDecoded, x, 1) = LCase(Mid(pDecoded, x, 1))
    End If
Next x

'now put in the proper spacing
arrCode = Split(CaseCode, " "): n = 1
For x = LBound(arrCode) To UBound(arrCode)
    CaseDecode = CaseDecode & Mid(pDecoded, _
       n, Len(arrCode(x))) & " "
    n = n + Len(arrCode(x))
Next x

CaseDecode = Trim(CaseDecode)
End Function


Private Function Decrypt(pEncoded As String, pKeyWord As String) As String
Dim x         As Integer
Dim xPos      As Integer
Dim yPos      As Integer
Dim lKeyPos   As Integer
Dim lCipher   As Integer
Dim lKeyLen   As Integer
Dim lKeyVal   As Integer
Dim lMsgVal   As Integer
Dim lLocation As Byte
Dim lKeyword  As String

Decrypt = ""
lKeyword = pKeyWord
lKeyPos = 1
lKeyLen = Len(lKeyword)
If lKeyLen > 0 Then
    'cycle through each character in the message
    For x = 1 To Len(pEncoded)
        'return to the start of the keyword if
        'shorter than the message
        If lKeyPos > lKeyLen Then lKeyPos = 1
        lKeyVal = Asc(Mid(lKeyword, lKeyPos, 1))
        lMsgVal = Asc(Mid(pEncoded, x, 1))
        'if the current message character is valid, decode it
        If lMsgVal >= START_ASCII And lMsgVal <= LAST_ASCII Then
            'cycle through each row in the matrix until the
            'value is found
            For yPos = 0 To UBound(Matrix)
                xPos = lKeyVal - START_ASCII
                lLocation = Matrix(xPos, yPos)
                If lLocation = lMsgVal Then
                    'the decoded character is the value
                    'of the row
                    Decrypt = Decrypt & Chr(yPos + START_ASCII)
                    'character found, stop searching
                    Exit For
                End If
            Next yPos
            lKeyPos = lKeyPos + 1
        Else
            Decrypt = Decrypt & Chr(lMsgVal)
            lKeyPos = lKeyPos + 1
        End If
    Next x
End If

End Function

Private Function Encrypt(pMessage As String, _
   pKeyWord As String) As String
Dim x        As Integer
Dim xPos     As Integer
Dim yPos     As Integer
Dim lKeyPos  As Integer
Dim lCipher  As Integer
Dim lKeyLen  As Integer
Dim lKeyVal  As Integer
Dim lMsgVal  As Integer
Dim lKeyword As String

Encrypt = ""
lKeyPos = 1

lKeyword = pKeyWord
lKeyLen = Len(lKeyword)
If lKeyLen > 0 Then
    'cycle through each character in the message
    For x = 1 To Len(pMessage)
        'return to the start of the keyword if shorter
        'than the message
        If lKeyPos > lKeyLen Then lKeyPos = 1
        'get the value of the ascii character
        lKeyVal = Asc(Mid(lKeyword, lKeyPos, 1))
        lMsgVal = Asc(Mid(pMessage, x, 1))
        'if the current message character is valid, encode it
        If lMsgVal >= START_ASCII And lMsgVal <= LAST_ASCII Then
            xPos = lKeyVal - START_ASCII
            yPos = lMsgVal - START_ASCII
            lCipher = Matrix(xPos, yPos)
            Encrypt = Encrypt & Chr(lCipher)
            lKeyPos = lKeyPos + 1
        Else
            Encrypt = Encrypt & Chr(lMsgVal)
            lKeyPos = lKeyPos + 1
        End If
    Next x
End If
End Function

    '##########################################
    '### Class Initialization & Termination ###


Private Sub Class_Initialize()
    'build the matrix
    BuildMatrix
End Sub

Private Sub Class_Terminate()
    'destroy everything here
End Sub

