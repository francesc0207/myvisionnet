VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFileTransfer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'-------------------------------------------------------------------------------------------
'M�DUL: CFileTransfer
'-------------------------------------------------------------------------------------------
'
'DESCRIPCI� GENERAL:
'--------------------
'D�na serveis de transfer�ncia segura de fitxers a un entorn web
'QUIM 13/6/2005: S'afegeix funcionalitat per publicaci� de documents p�blics
'
'COMENTARIS T�CNICS
'--------------------
'
'--------------------------------------------------------------------------------------------


Option Explicit


Public Function TrimPath(ByVal asPath As String, ByRef sError As Variant) As String
  On Error GoTo catch
    If Len(asPath) = 0 Then Exit Function
    Dim x As Integer
    
    Do
        x = InStr(asPath, "\")
        If x = 0 Then Exit Do
        asPath = Right(asPath, Len(asPath) - x)
    Loop
    TrimPath = asPath
    Exit Function
catch:
    TrimPath = ""
    sError = "Error en CTransferFile.TrimPath Causa: " & Err.Number & "-" & Err.Description
    Exit Function
End Function

Public Function FileExist(ByVal asPath As String, ByRef sError As Variant) As Boolean
    On Error GoTo catch
    
        If UCase(Dir(asPath)) = UCase(TrimPath(asPath, sError)) Then
          FileExist = True
        Else
          FileExist = False
        End If
    Exit Function
    
catch:
    FileExist = False
    sError = "Error en CTransferFile.FileExist Causa: " & Err.Number & "-" & Err.Description
    Exit Function
End Function
Public Function ReadBinFile(ByVal filename As String, ByRef sError As Variant) As Boolean

       Dim fl As Long
       Dim binbyte() As Byte
       Dim binfilestr As String
 
       On Error GoTo errHandler
        
       If Not FileExist(filename, sError) Then
            ReadBinFile = False
            sError = "No se ha podido obtener el fichero " & filename
            Exit Function
       End If
        
       Open filename For Binary Access Read Shared As #1
 
       fl = FileLen(filename)
       ReDim binbyte(fl)
 
       Get #1, , binbyte
 
       Close #1
 
       ReadBinFile = True
       Exit Function
 
errHandler:
    sError = "Error en CTransferFile.ReadBinFile Causa: " & Err.Number & "-" & Err.Description
    ReadBinFile = False
    Exit Function
End Function


Public Function ResponseBinaryWrite(ByVal filename As String, ByVal Response As Object, ByRef sError As Variant)

       Dim fl As Long
       Dim binbyte() As Byte
       Dim binfilestr As String
 
       On Error GoTo errHandler
       
       If Not FileExist(filename, sError) Then
            ResponseBinaryWrite = False
            Exit Function
       End If
 
       Open filename For Binary Access Read Shared As #1
 
       fl = FileLen(filename)
       ReDim binbyte(fl)
 
       Get #1, , binbyte
 
       Close #1
 
       Response.Binarywrite binbyte

       ResponseBinaryWrite = True
       Exit Function
 
errHandler:
       sError = "Error en CTransferFile.ResponseBinaryWrite Causa: " & Err.Number & "-" & Err.Description
       ResponseBinaryWrite = False
       Exit Function
End Function

Public Function ResponseBinaryWriteStream(ByVal filename As String, ByVal Response As Object, ByRef sError As Variant)

       Dim OStream As Object
 
       On Error GoTo errHandler

       If Not FileExist(filename, sError) Then
            ResponseBinaryWriteStream = False
            Exit Function
       End If
       
       Set OStream = CreateObject("ADODB.Stream")
       
       OStream.Open
       
       OStream.Type = 1 'Binary type
       
       OStream.LoadFromFile filename
       
       Response.Binarywrite OStream.Read

       ResponseBinaryWriteStream = True
       
       Set OStream = Nothing
       
       Exit Function
 
errHandler:
       Set OStream = Nothing
       sError = "Error en CTransferFile.ResponseBinaryWriteStream Causa: " & Err.Number & "-" & Err.Description
       ResponseBinaryWriteStream = False
       Exit Function
End Function




