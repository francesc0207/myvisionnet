Attribute VB_Name = "Functions"
Option Explicit

Private Type GUID
    Data1 As Long
    Data2 As Integer
    Data3 As Integer
    Data4(7) As Byte
End Type

Private Declare Function CoCreateGuid Lib "OLE32.DLL" (pGuid As GUID) As Long

Public Function GetGUID() As String


    Dim udtGUID As GUID

    If (CoCreateGuid(udtGUID) = 0) Then

        GetGUID = _
           String(8 - Len(Hex$(udtGUID.Data1)), "0") & Hex$(udtGUID.Data1) & _
           String(4 - Len(Hex$(udtGUID.Data2)), "0") & Hex$(udtGUID.Data2) & _
           String(4 - Len(Hex$(udtGUID.Data3)), "0") & Hex$(udtGUID.Data3) & _
           IIf((udtGUID.Data4(0) < &H10), "0", "") & Hex$(udtGUID.Data4(0)) & _
           IIf((udtGUID.Data4(1) < &H10), "0", "") & Hex$(udtGUID.Data4(1)) & _
           IIf((udtGUID.Data4(2) < &H10), "0", "") & Hex$(udtGUID.Data4(2)) & _
           IIf((udtGUID.Data4(3) < &H10), "0", "") & Hex$(udtGUID.Data4(3)) & _
           IIf((udtGUID.Data4(4) < &H10), "0", "") & Hex$(udtGUID.Data4(4)) & _
           IIf((udtGUID.Data4(5) < &H10), "0", "") & Hex$(udtGUID.Data4(5)) & _
           IIf((udtGUID.Data4(6) < &H10), "0", "") & Hex$(udtGUID.Data4(6)) & _
           IIf((udtGUID.Data4(7) < &H10), "0", "") & Hex$(udtGUID.Data4(7))
    End If

End Function

'Aquesta funci� ens independitza de la configuraci� regional
'Cal tenir en compte que si la configuraci� regional t� el punt (.) com a separador de dates
'llavors la funci� Format(now,"dd/mm/yyyy hh:mm:ss") pot retornar 11/12/2004 07.02.04 i aix� �s un problema perque a SQLServer i d'altres no s'ho empassen
'Per aix� hem hagut de fer aquesta funci�
Public Function ConvertToIsoDateTime(fecha As Date) As String
On Error GoTo catch
    ConvertToIsoDateTime = ConvertToIsoDateOnly(fecha) & " " & ConvertToIsoHour(fecha)
    Exit Function
catch:
    'MsgBox Error
End Function

Public Function ConvertToIsoDateOnly(fecha As Date) As String
On Error GoTo catch
    ConvertToIsoDateOnly = Year(fecha) & ConvertToDoubleDigit(Month(fecha)) & ConvertToDoubleDigit(Day(fecha))
    
    Exit Function
catch:
    'MsgBox Error
End Function

Public Function ConvertToIsoHour(fecha As Date) As String
On Error GoTo catch
    ConvertToIsoHour = ConvertToDoubleDigit(Hour(fecha)) & ":" & ConvertToDoubleDigit(Minute(fecha)) & ":" & ConvertToDoubleDigit(Second(fecha))
    Exit Function
catch:
    'MsgBox Error
End Function

'Si el n� �s de 1 xifra, afegim un 0 al davant
Private Function ConvertToDoubleDigit(ByVal Value As Long) As String
    ConvertToDoubleDigit = Right(CStr(Value + 100), 2)
End Function
Public Function IsInStringArray(ByVal sValueToFind As String, ByVal sStringArray As String) As Boolean
    Dim sValue As Variant
    
    On Error GoTo catch
    
    For Each sValue In Split(sStringArray, ",")
        If sValueToFind = sValue Then
            IsInStringArray = True
            Exit Function
        End If
    Next sValue

Finally:
    IsInStringArray = False
    Exit Function
catch:
    Resume Finally
End Function
Public Function vFilterString(rvntFieldVal As Variant) As Variant
  If IsNull(rvntFieldVal) Then
    vFilterString = vbNullString
  Else
    vFilterString = CStr(rvntFieldVal)
  End If
End Function

'------------------------------------------------------------
'returns "" if a field is Null
'------------------------------------------------------------
Function vFilterNum(rvntFieldVal As Variant) As Long
  If IsNull(rvntFieldVal) Then
    vFilterNum = 0
  Else
    vFilterNum = rvntFieldVal
  End If
End Function

Public Function ConvertDateToJulian(fecha As Date) As Long

    On Error GoTo catch

    Dim TempFromDate As Date
    Dim TempString As String ' holds the date of 1/1/ of this year
    Dim TempYr As Double
    Dim fromdate As Long
    
    TempFromDate = DateValue(fecha)
    TempYr = 100 + Val(Format(TempFromDate, "yyyy")) - Val(Format(DateValue("01/01/2000"), "yyyy"))    'VBLM SKIP=3
       
    TempYr = TempYr * 1000 ' get it to the beginning of the number
    TempString = "01/01/" + Format(TempFromDate, "yyyy")
    fromdate = TempYr + TempFromDate - DateValue(TempString) + 1
    
    ConvertDateToJulian = fromdate '(Results = 100015)
    Exit Function
catch:
    ConvertDateToJulian = 0

End Function

Public Function PreviousMonthNumber(moment As Date) As Integer

    If Month(moment) > 1 Then
        PreviousMonthNumber = Month(moment) - 1
    Else
        PreviousMonthNumber = 12
    End If

End Function
Public Function NombreMes(mesNum As Integer, LANG As String) As String

'VBLM OFF
    Select Case LANG
            Case "ESP"
                Select Case mesNum
                    Case 1
                        NombreMes = "Enero"
                    Case 2
                        NombreMes = "Febrero"
                    Case 3
                        NombreMes = "Marzo"
                    Case 4
                        NombreMes = "Abril"
                    Case 5
                        NombreMes = "Mayo"
                    Case 6
                        NombreMes = "Junio"
                    Case 7
                        NombreMes = "Julio"
                    Case 8
                        NombreMes = "Agosto"
                    Case 9
                        NombreMes = "Septiembre"
                    Case 10
                        NombreMes = "Octubre"
                    Case 11
                        NombreMes = "Noviembre"
                    Case 12
                        NombreMes = "Diciembre"
                End Select
            Case "ENG"
                Select Case mesNum
                    Case 1
                        NombreMes = "January"
                    Case 2
                        NombreMes = "February"
                    Case 3
                        NombreMes = "March"
                    Case 4
                        NombreMes = "April"
                    Case 5
                        NombreMes = "May"
                    Case 6
                        NombreMes = "June"
                    Case 7
                        NombreMes = "July"
                    Case 8
                        NombreMes = "August"
                    Case 9
                        NombreMes = "September"
                    Case 10
                        NombreMes = "October"
                    Case 11
                        NombreMes = "November"
                    Case 12
                        NombreMes = "December"
                End Select
            Case "CAT"
                Select Case mesNum
                    Case 1
                        NombreMes = "Gener"
                    Case 2
                        NombreMes = "Febrer"
                    Case 3
                        NombreMes = "Mar�"
                    Case 4
                        NombreMes = "Abril"
                    Case 5
                        NombreMes = "Maig"
                    Case 6
                        NombreMes = "Juny"
                    Case 7
                        NombreMes = "Juliol"
                    Case 8
                        NombreMes = "Agost"
                    Case 9
                        NombreMes = "Setembre"
                    Case 10
                        NombreMes = "Octubre"
                    Case 11
                        NombreMes = "Novembre"
                    Case 12
                        NombreMes = "Desembre"
                End Select
    End Select

'VBLM ON

End Function


Public Function NombreDiaSemana(fecha As Date, LANG As String) As String

    On Error GoTo catch
    
    Select Case LANG
        Case "ESP"
            Select Case Weekday(fecha, vbMonday)
                Case vbSunday
                    NombreDiaSemana = "Domingo"
                Case vbMonday
                    NombreDiaSemana = "Lunes"
                Case vbTuesday
                    NombreDiaSemana = "Martes"
                Case vbWednesday
                    NombreDiaSemana = "Mi�rcoles"
                Case vbThursday
                    NombreDiaSemana = "Jueves"
                Case vbFriday
                    NombreDiaSemana = "Viernes"
                Case vbSaturday
                    NombreDiaSemana = "S�bado"
                Case Else
                    NombreDiaSemana = "???"
            End Select
        Case "CAT"
            Select Case Weekday(fecha, vbMonday)
                Case vbSunday
                    NombreDiaSemana = "Diumenge"
                Case vbMonday
                    NombreDiaSemana = "Dilluns"
                Case vbTuesday
                    NombreDiaSemana = "Dimarts"
                Case vbWednesday
                    NombreDiaSemana = "Dimecres"
                Case vbThursday
                    NombreDiaSemana = "Dijous"
                Case vbFriday
                    NombreDiaSemana = "Divendres"
                Case vbSaturday
                    NombreDiaSemana = "Dissabte"
                Case Else
                    NombreDiaSemana = "???"
            End Select
        Case "ENG"
            Select Case Weekday(fecha, vbMonday)
                Case vbSunday
                    NombreDiaSemana = "Sunday"
                Case vbMonday
                    NombreDiaSemana = "Monday"
                Case vbTuesday
                    NombreDiaSemana = "Tuesday"
                Case vbWednesday
                    NombreDiaSemana = "Wednesday"
                Case vbThursday
                    NombreDiaSemana = "Thursday"
                Case vbFriday
                    NombreDiaSemana = "Friday"
                Case vbSaturday
                    NombreDiaSemana = "Saturday"
                Case Else
                    NombreDiaSemana = "???"
            End Select
    End Select
    Exit Function
catch:
    NombreDiaSemana = ""
End Function
Function FirstDateOfMonth(Mes As Integer, Anyo As Integer) As Date

    FirstDateOfMonth = CDate("1/" & Mes & "/" & Anyo)

End Function
Function LastDateOfMonth(Mes As Integer, Anyo As Integer) As Date

    Dim ultimodia As Integer
    
    ultimodia = 31
    While ultimodia > 0 And Not IsDate(ultimodia & "/" & Mes & "/" & Anyo)
        ultimodia = ultimodia - 1
    Wend
    If ultimodia > 0 Then
        LastDateOfMonth = CDate(ultimodia & "/" & Mes & "/" & Anyo)
    Else
        LastDateOfMonth = 0
    End If

End Function


Public Function CurrentMonthFirstDate(moment As Date) As Date
    CurrentMonthFirstDate = FirstDateOfMonth(Month(moment), Year(moment))
End Function
Public Function CurrentMonthLastDate(moment As Date) As Date
    CurrentMonthLastDate = LastDateOfMonth(Month(moment), Year(moment))
End Function

Public Function PreviousMonthFirstDate(moment As Date) As Date

    Dim currentMonth As Integer
    Dim previousMonth As Integer
    Dim previousMonthYear As Integer

    currentMonth = Month(moment)
    If currentMonth > 1 Then
        previousMonth = currentMonth - 1
        previousMonthYear = Year(moment)
    Else
        previousMonth = 12
        previousMonthYear = Year(moment) - 1
    End If
    PreviousMonthFirstDate = FirstDateOfMonth(previousMonth, previousMonthYear)

End Function

Public Function PreviousMonthLastDate(moment As Date) As Date

    Dim currentMonth As Integer
    Dim previousMonth As Integer
    Dim previousMonthYear As Integer
    Dim ultimodia As Integer

    currentMonth = Month(moment)
    If currentMonth > 1 Then
        previousMonth = currentMonth - 1
        previousMonthYear = Year(moment)
    Else
        previousMonth = 12
        previousMonthYear = Year(moment) - 1
    End If
    
    PreviousMonthLastDate = LastDateOfMonth(previousMonth, previousMonthYear)
    

End Function
